<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Load Property
 * @property CI_Upload $upload
 */
class Inbox extends MY_Controller
{
    protected $table = 'messages';

    public function __construct()
    {
        parent::__construct();
        if (! $this->is_logged_in()) {
            to_login();
        }

        $this->load->helper(array(
            'form', 'auth'
        ));
    }

    /**
     * Home Inbox
     */
    public function index()
    {
        $this->load->library('form_validation');
        $data['send_mail_url'] = site_url('inbox/send-message', $this->use_ssl);
        $this->load->view('users/inbox/inbox_form', $data);
    }

    /**
     *
     */
    public function send_message()
    {
        if ( ! $this->auth_user_id ) {
            show_404();
        }

        $this->load->library('form_validation');

        /**
         * Upload Handle
         */
        $config = [
            'upload_path'  => FCPATH . '/media/attachment/',
            'allowed_types' => 'gif|jpg|png',
            'file_ext_tolower' => true,
        ];

        $this->load->library('upload', $config);

        $image_name = '';
        if ( $_FILES['upload']['error'] == 0 && $this->upload->do_upload('upload')) {
            $image = $this->upload->data();
            $image_name = $image['file_name'];
        }

        /**
         * Validation
         */
        $user_id = $this->auth_user_id;

        $user_data = [
            'sender_id' => $user_id,
            'date_time' => date('Y-m-d H:i:s'),
            'category'  => $this->input->post('category'),
            'title'     => $this->input->post('title'),
            'content'   => $this->input->post('message')
        ];

        $this->form_validation->set_data( $user_data );
        $validation_rules = [
            [
                'field' => 'title', // field (key)
                'label' => 'Judul Pesan',
                'rules' => 'required|trim',
                'errors' => [
                    'required' => 'Judul Pesan Wajib diisi'
                ]
            ],
            [
                'field' => 'category', // field (key)
                'label' => 'Kategori Pesan',
                'rules' => 'required',
                'errors' => [
                    'required' => 'Kategori Wajib dipilih'
                ]
            ]
        ];

        $this->form_validation->set_rules( $validation_rules );
        if ( $this->form_validation->run() ) {
            $user_data['attachment'] = $image_name;

            $insert = $this->db->set($user_data)
                ->insert($this->table);

            if ($insert) {
                /**
                 * Email Handle
                 */
                send_mail([
                    'subject' => $user_data['title'],
                    'to'      => get_option('email'),
                    'message' => '<p>' . $user_data['content'] . '</p>',
                ]);
            }

            echo '<p>Email Terkirim <a href="'. site_url('inbox', $this->use_ssl) .'"></a></p>';

        } else {
            echo '<h1>Inbox Creation Error(s)</h1>' . validation_errors();
        }

    }

}
