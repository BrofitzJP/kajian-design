<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Class User
 *
 * @property Authentication $authentication
 * @property Users $users
 * @property tokens $tokens
 */
class User extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array(
            'form', 'auth'
        ));
    }

    /**
     * Login
     */
    public function login()
    {
        // Method should not be directly accessible
        if ($this->uri->uri_string() == 'user/login') {
            $this->not_found();
        }

        if (strtolower($_SERVER['REQUEST_METHOD']) == 'post') {
            $this->require_min_level(1);
        }

        $this->setup_login_form();

        $html = $this->load->view('users/auth/login', '', TRUE);

        echo $html;
    }

    /**
     * Log out
     */
    public function logout()
    {
        $this->authentication->logout();

        redirect(site_url(LOGIN_PAGE . '?' . 'redirect=user', $this->use_ssl));
    }

    /**
     * Forgot and Reset Password
     */
    public function forgot_password()
    {
        // Load resources
        $this->load->model('users');
        /// If IP or posted email is on hold, display message
        if ($on_hold = $this->authentication->current_hold_status(TRUE)) {
            $view_data['disabled'] = 1;
        } else {
            // If the form post looks good
            if ($this->tokens->match && $this->input->post('email')) {
                if ($user_data = $this->users->get_recovery_data($this->input->post('email'))) {
                    // Check if user is banned
                    if ($user_data->banned == '1') {
                        // Log an error if banned
                        $this->authentication->log_error($this->input->post('email', TRUE));

                        // Show special message for banned user
                        $view_data['banned'] = 1;
                    } else {
                        /**
                         * Use the authentication libraries salt generator for a random string
                         * that will be hashed and stored as the password recovery key.
                         * Method is called 4 times for a 88 character string, and then
                         * trimmed to 72 characters
                         */
                        $recovery_code = substr($this->authentication->random_salt()
                            . $this->authentication->random_salt()
                            . $this->authentication->random_salt()
                            . $this->authentication->random_salt(), 0, 72);

                        // Update user record with recovery code and time
                        $this->users->update_user_raw_data(
                            $user_data->user_id,
                            [
                                'passwd_recovery_code' => $this->authentication->hash_passwd($recovery_code),
                                'passwd_recovery_date' => date('Y-m-d H:i:s')
                            ]
                        );

                        // Set URI of link
                        $link_uri = 'user/recovery-pass-code/' . $user_data->user_id . '/' . $recovery_code;

                        $link_reset = anchor(
                            site_url($link_uri, $this->use_ssl),
                            site_url($link_uri, $this->use_ssl),
                            'target ="_blank"'
                        );


                        send_mail([
                            'to' => $this->input->post('email'),
                            'subject' => 'Recovery Password',
                            'message' => '<p>Klik link berikut untuk me-reset password :</p><p>' . $link_reset . '</p>'
                        ]);

                        $view_data['confirmation'] = 1;
                    }
                } // There was no match, log an error, and display a message
                else {
                    // Log the error
                    $this->authentication->log_error($this->input->post('email', TRUE));

                    $view_data['no_match'] = 1;
                }
            }
        }

        $this->load->view('users/auth/recovery_form', (isset($view_data)) ? $view_data : '');
    }

    /**
     * @param $user_id
     * @param $recovery_code
     */
    public function recovery_pass_code($user_id, $recovery_code)
    {
        /// If IP is on hold, display message
        if ($on_hold = $this->authentication->current_hold_status(TRUE)) {
            $view_data['disabled'] = 1;
        } else {
            // Load resources
            $this->load->model('users');
            /**
             * Try to get a hashed password recovery
             * code and user salt for the user.
             * @var object|false
             */
            $recovery_data =
                /**
                 * Make sure that $user_id is a number and less
                 * than or equal to 10 characters long
                 */
                is_numeric($user_id) && strlen($user_id) <= 10
                /**
                 * Make sure that $recovery code is exactly 72 characters long
                 */
                && strlen($recovery_code) === 72
                ? $this->users->get_recovery_verification_data($user_id)
                : false;
            if (is_object($recovery_data) && isset($recovery_data->passwd_recovery_code)) {
                /**
                 * Check that the recovery code from the
                 * email matches the hashed recovery code.
                 */
                if ($recovery_data->passwd_recovery_code == $this
                        ->authentication
                        ->check_passwd(
                            $recovery_data->passwd_recovery_code,
                            $recovery_code
                        )
                ) {
                    $view_data['user_id'] = $user_id;
                    $view_data['username'] = $recovery_data->username;
                    $view_data['recovery_code'] = $recovery_data->passwd_recovery_code;
                } // Link is bad so show message
                else {
                    $view_data['recovery_error'] = 1;

                    // Log an error
                    $this->authentication->log_error('');
                }
            } // Link is bad so show message
            else {
                $view_data['recovery_error'] = 1;

                // Log an error
                $this->authentication->log_error('');
            }

            /**
             * If form submission is attempting to change password
             */
            if ($this->tokens->match) {
                $this->users->recovery_password_change();
            }
        }

        $this->load->view('users/auth/choose_password', $view_data);
    }

    /**
     * Home Users
     */
    public function index()
    {
//        send_mail([
//            'to' => 'wichaksono.w@gmail.com',
//            'message' => '<p>Sedang Kirim Pesan <b>Email</b></p>'
//        ]);

        if ($this->verify_role('customer')) {

            echo '<h2>Welcome ' . $this->auth_role . '</h2>';
            $result = $this->general->get_kajian();
            print_r($result);

            primary_navigation();
        } else {
            redirect('login?redirect=user');
        }
    }

    /**
     * Profile Page
     */
    public function profile($username = null)
    {
        if ($this->is_logged_in() &&
            strpos($this->uri->uri_string(), 'user/profile') === false &&
            $username != null
        ) {
            if (! empty($this->auth_user_id)) {
                $users = $this->general->get_users([
                    'where' => [
                        [
                            'field' => 'username',
                            'value' => $username
                        ]
                    ]
                ]);
                print_r($users);
                echo '<br/>';
                echo anchor(site_url('user/edit/', $this->use_ssl), 'Edit');
            }
        } else {
            $this->not_found();
        }

    }

    /**
     * Edit Current User
     */
    public function edit()
    {
        if (! $this->is_logged_in()) {
            show_404();
        }

        $user_id = $this->auth_user_id;
        $users = $this->general->get_users([
            'where' => [
                [
                    'field' => 'user_id',
                    'value' => $user_id
                ]
            ]
        ]);

        $view_data['users'] = $users;
        $view_data['actions'] = site_url('user/update', $this->use_ssl);

        $this->load->view('users/auth/edit', $view_data);
    }

    /**
     * Update Current User
     */
    public function update()
    {
        $this->load->model('users');
        $this->load->library('form_validation');

        $user_data = [
            'email' => $this->input->post('email'),
            'sex' => $this->input->post('sex'),
            'display_name' => $this->input->post('name'),
            'born' => $this->input->post('date'),
        ];

        $this->form_validation->set_data($user_data);

        $validation_rules = [
            [
                'field' => 'display_name', // field (key)
                'label' => 'name', // input
                'rules' => 'required|trim',
                'errors' => [
                    'required' => 'Nama Lengkap Wajib diisi'
                ]
            ],
            [
                'field' => 'email',
                'label' => 'email',
                'rules' => 'trim|required|valid_email',
                'errors' => [
                    'required' => 'Alamat Email Wajib diisi'
                ]
            ],
            [
                'field' => 'born',
                'label' => 'date',
                'rules' => 'required',
                'errors' => [
                    'required' => 'Tanggal Lahir Wajib diisi'
                ]
            ],
            [
                'field' => 'sex',
                'label' => 'sex',
                'rules' => 'required',
                'errors' => [
                    'required' => 'Jenis Kelamin Wajib diisi'
                ]
            ],
        ];

        $this->form_validation->set_rules($validation_rules);

        if ($this->form_validation->run()) {
            $password = $this->input->post('password');
            $repassword = $this->input->post('re-password');

            if ($password) {
                if ( $password!=$repassword ) {
                    redirect(site_url('user/edit/?update=error&pass=not-match', $this->use_ssl));
                } else {
                    $passwd = $this->input->post('password');
                    $user_data['passwd'] = $this->authentication->hash_passwd($password);
                }
            }

            $user_data['address'] = $this->input->post('address');
            $user_data['modified_at'] = date('Y-m-d H:i:s');

            $user_id = $this->input->post('user_id');

            $this->db->where('user_id', $user_id);
            $update = $this->db->update('users', $user_data);

            if ($update) {
                echo '<h1>Congratulations</h1>' . '<p>User ' . $this->general->get_user_by_ID($user_id, 'username') . ' was UPDATED.</p>';
            }

        } else {
            echo '<h1>User Creation Error(s)</h1>' . validation_errors();
        }
    }

    /**
     * REGISTER Handle
     */
    public function register()
    {
        $data['validation_url'] = site_url('user/add-new-member');
        $this->load->view('users/auth/register', $data);
    }

    public function add_new_member()
    {
        $this->load->model('users');
        $this->load->library('form_validation');

        $user_data = [
            'username' => $this->input->post('username'),
            'passwd' => $this->input->post('password'),
            'email' => $this->input->post('email'),
            'auth_level' => '1',
            'tos' => $this->input->post('tos'),
            'sex' => $this->input->post('sex'),
            'display_name' => $this->input->post('name'),
            'born' => $this->input->post('date'),
            'repassword' => $this->input->post('re-password'),
        ];


        $this->form_validation->set_data($user_data);

        $validation_rules = [
            [
                'field' => 'display_name', // field (key)
                'label' => 'name', // input
                'rules' => 'required|trim',
                'errors' => [
                    'required' => 'Nama Lengkap Wajib diisi'
                ]
            ],
            [
                'field' => 'username',
                'label' => 'username',
                'rules' => 'max_length[12]|is_unique[' . db_table('user_table') . '.username]',
                'errors' => [
                    'is_unique' => 'Usernama Sudah digunakan'
                ]
            ],
            [
                'field' => 'passwd',
                'label' => 'passwd',
                'rules' => [
                    'trim',
                    'required',
                    [
                        '_check_password_strength',
                        [$this->users, '_check_password_strength']
                    ]
                ],
                'errors' => [
                    'required' => 'Kolom Password Wajib diisi'
                ]
            ],
            [
                'field' => 'repassword',
                'label' => 'Re Password',
                'rules' => [
                    'trim',
                    'required',
                    'matches[passwd]'
                ],
                'errors' => [
                    'required' => 'Password Wajib di isi',
                    'matches' => 'Password Tidak Sama'
                ]
            ],
            [
                'field' => 'email',
                'label' => 'email',
                'rules' => 'trim|required|valid_email|is_unique[' . db_table('user_table') . '.email]',
                'errors' => [
                    'is_unique' => 'Alamat Email sudah digunakan'
                ]
            ],
            [
                'field' => 'auth_level',
                'label' => 'auth_level',
                'rules' => 'required|integer|in_list[1,6,9]'
            ],
            [
                'field' => 'born',
                'label' => 'date',
                'rules' => 'required',
                'errors' => [
                    'required' => 'Tanggal Lahir Wajib diisi'
                ]
            ],
            [
                'field' => 'sex',
                'label' => 'sex',
                'rules' => 'required',
                'errors' => [
                    'required' => 'Jenis Kelamin Wajib diisi'
                ]
            ],
            [
                'field' => 'tos',
                'label' => 'tos',
                'rules' => 'required',
                'errors' => [
                    'required' => 'TOS checkbox Wajib diisi'
                ]
            ],
        ];


        $this->form_validation->set_rules($validation_rules);

        if ($this->form_validation->run()) {
            $user_data['passwd'] = $this->authentication->hash_passwd($user_data['passwd']);
            $user_data['user_id'] = $this->users->get_unused_id();
            $user_data['address'] = $this->input->post('address');
            $user_data['created_at'] = date('Y-m-d H:i:s');
            // disable first account
            $user_data['banned'] = '1';
            $user_data['activation_code'] = md5(microtime() . md5($user_data['created_at']));

            unset($user_data['tos']);
            unset($user_data['repassword']);


            // If username is not used, it must be entered into the record as NULL
            if (empty($user_data['username'])) {
                $user_data['username'] = NULL;
            }

            $this->db->set($user_data)
                ->insert(db_table('user_table'));

            if ($this->db->affected_rows() == 1) {
                /**
                 * Kirim Email
                 */
                $message = '<h3>Selamat ' . $user_data['username'] . '</h3>';
                $message .= '<p>Tinggal Selangkah lagi untuk menggunakan Akun Anda. Klik link berikut :</p>';
                $message .= sprintf('<p><a href="%s">%s</a></p>',
                    site_url('user/activations/' . $user_data['activation_code']),
                    'Aktifkan Akun'
                );

                // ------------------
                // send email
                // ------------------
                send_mail([
                    'to' => $user_data['email'],
                    'message' => $message
                ]);
                echo '<h1>Congratulations</h1>' . '<p>User ' . $user_data['username'] . ' was created.</p>';
                echo '<p>Cek Email ' . $user_data['email'] . ' untuk melakukan Aktifasi Akun</p>';
            }

        } else {
            echo '<h1>User Creation Error(s)</h1>' . validation_errors();
        }

    }

    /**
     * validasi kode registrasi
     * @param $code
     */
    public function activation($code)
    {
        $this->load->model('general');
        $active = $this->general->_code_is_exist($code);
        if ($active) {
            echo sprintf('<h4>Selamat akun Anda Sudah aktif, silahkan <a href="%s">Login</a>',
                site_url('user', $this->use_ssl)
            );
        } else {
            $this->not_found();
        }
    }

    /**
     * AJAX Handle Register
     */

    /**
     * count password meter using ajax
     * @return string Strong | HTML error
     */
    public function password_meter()
    {
        $is_ajax = $this->input->is_ajax_request();

        if (!$is_ajax) {
            $this->not_found();
        }

        $this->config->load('examples/password_strength');

        $password = $this->input->post('password');

        // Password length
        $max = config_item('max_chars_for_password') > 0
            ? config_item('max_chars_for_password')
            : '';

        $regex = '(?=.{' . config_item('min_chars_for_password') . ',' . $max . '})';
        $error = '<li>At least ' . config_item('min_chars_for_password') . ' characters</li>';

        if (config_item('max_chars_for_password') > 0) {
            $error .= '<li>Not more than ' . config_item('max_chars_for_password') . ' characters</li>';
        }

        // Digit(s) required
        if (config_item('min_digits_for_password') > 0) {
            $regex .= '(?=(?:.*[0-9].*){' . config_item('min_digits_for_password') . ',})';
            $plural = config_item('min_digits_for_password') > 1 ? 's' : '';
            $error .= '<li>' . config_item('min_digits_for_password') . ' number' . $plural . '</li>';
        }

        // Lower case letter(s) required
        if (config_item('min_lowercase_chars_for_password') > 0) {
            $regex .= '(?=(?:.*[a-z].*){' . config_item('min_lowercase_chars_for_password') . ',})';
            $plural = config_item('min_lowercase_chars_for_password') > 1 ? 's' : '';
            $error .= '<li>' . config_item('min_lowercase_chars_for_password') . ' lower case letter' . $plural . '</li>';
        }

        // Upper case letter(s) required
        if (config_item('min_uppercase_chars_for_password') > 0) {
            $regex .= '(?=(?:.*[A-Z].*){' . config_item('min_uppercase_chars_for_password') . ',})';
            $plural = config_item('min_uppercase_chars_for_password') > 1 ? 's' : '';
            $error .= '<li>' . config_item('min_uppercase_chars_for_password') . ' upper case letter' . $plural . '</li>';
        }

        // Non-alphanumeric char(s) required
        if (config_item('min_non_alphanumeric_chars_for_password') > 0) {
            $regex .= '(?=(?:.*[^a-zA-Z0-9].*){' . config_item('min_non_alphanumeric_chars_for_password') . ',})';
            $plural = config_item('min_non_alphanumeric_chars_for_password') > 1 ? 's' : '';
            $error .= '<li>' . config_item('min_non_alphanumeric_chars_for_password') . ' non-alphanumeric character' . $plural . '</li>';
        }

        if (preg_match('/^' . $regex . '.*$/', $password)) {
            die('Strong');
        }

        die($error);
    }

    public function username_is_exist()
    {
        $is_ajax = $this->input->is_ajax_request();
        $username = $this->input->get('username');
        if (!$is_ajax) {
            $this->not_found();
        }

        $query = $this->db->get_where('users', [
            'username' => $username
        ]);

        $result = [
            'error' => 'Invalid Respond'
        ];
        if ($query->num_rows() >= 0) {
            $result = [
                'error' => 0,
                'status'=> $query->num_rows()
            ];
        }

        die(json_encode($result, JSON_PRETTY_PRINT));

    }

    /**
     * email
     * @return string | JSON
     */
    public function email_is_exist()
    {
        $is_ajax = $this->input->is_ajax_request();

        if (!$is_ajax) {
            show_404();
        }

        $email   = $this->input->get('email');

        $query   = $this->db->get_where('users', [
            'email' => $email
        ]);

        $result = [
            'error' => 'Invalid Query'
        ];

        if ($query->num_rows() >= 0) {
            $result = [
                'error'  => 0,
                'result' => $query->num_rows()
            ];
        }

        die(json_encode($result, JSON_PRETTY_PRINT));
    }

    /**
     * Utillity
     */
    public function not_found()
    {
        show_404();
    }

}