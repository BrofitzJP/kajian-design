<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Frontpage extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	// -------------------
	// Kajian
	public function index()
	{
		$data['contents']  = "frontpage/kajian";
		$data['url']       = uri_string();
		$this->load->view('frontpage/partials/wrapper' , $data);
	}

	// Edit Kajian
	public function edit_kajian()
	{
		$data['contents']  = "frontpage/edit_kajian";
		$data['url']       = uri_string();
		$this->load->view('frontpage/partials/wrapper' , $data);
	}

	// Kajian Result
	public function result_kajian()
	{
		$data['contents']  = "frontpage/result_kajian";
		$data['url']       = uri_string();
		$this->load->view('frontpage/partials/wrapper' , $data);
	}

	// -------------------
	// Ustadz
	public function ustadz()
	{
		$data['contents']  = "frontpage/ustadz";
		$data['url']       = uri_string();
		$this->load->view('frontpage/partials/wrapper' , $data);
	}

	// Ustadz Result
	public function result_ustadz()
	{
		$data['contents']  = "frontpage/result_ustadz";
		$data['url']       = uri_string();
		$this->load->view('frontpage/partials/wrapper' , $data);
	}

	// Detail Ustadz
	public function detail_ustadz()
	{
		$data['contents']  = "frontpage/detail_ustadz";
		$data['url']       = uri_string();
		$this->load->view('frontpage/partials/wrapper' , $data);
	}

	// Edit Ustadz
	public function edit_ustadz()
	{
		$data['contents']  = "frontpage/edit_ustadz";
		$data['url']       = uri_string();
		$this->load->view('frontpage/partials/wrapper' , $data);
	}

	// -------------------
	// Masjid
	public function masjid()
	{
		$data['contents']  = "frontpage/masjid";
		$data['url']       = uri_string();
		$this->load->view('frontpage/partials/wrapper' , $data);
	}

	// Add Masjid
	public function edit_masjid()
	{
		$data['contents']  = "frontpage/edit_masjid";
		$data['url']       = uri_string();
		$this->load->view('frontpage/partials/wrapper' , $data);
	}

	// Detail Masjid
	public function detail_masjid()
	{
		$data['contents']  = "frontpage/detail_masjid";
		$data['url']       = uri_string();
		$this->load->view('frontpage/partials/wrapper' , $data);
	}

	// -------------------
	// Donasi
	public function donasi()
	{
		$data['contents']  = "frontpage/donasi";
		$data['url']       = uri_string();
		$this->load->view('frontpage/partials/wrapper' , $data);
	}

	// -------------------
	// Profile
	public function user()
	{
		$data['contents']  = "frontpage/profile";
		$data['url']       = uri_string();
		$this->load->view('frontpage/partials/wrapper' , $data);
	}

	// Edit profile
	public function edit_user()
	{
		$data['contents']  = "frontpage/edit_profile";
		$data['url']       = uri_string();
		$this->load->view('frontpage/partials/wrapper' , $data);
	}

	// User kajian
	public function user_kajian()
	{
		$data['contents']  = "frontpage/user_kajian";
		$data['url']       = uri_string();
		$this->load->view('frontpage/partials/wrapper' , $data);
	}

	// User ustadz
	public function user_ustadz()
	{
		$data['contents']  = "frontpage/user_ustadz";
		$data['url']       = uri_string();
		$this->load->view('frontpage/partials/wrapper' , $data);
	}

	// User masjid
	public function user_masjid()
	{
		$data['contents']  = "frontpage/user_masjid";
		$data['url']       = uri_string();
		$this->load->view('frontpage/partials/wrapper' , $data);
	}

	// -------------------
	// Support
	public function support()
	{
		$data['contents']  = "frontpage/support";
		$data['url']       = uri_string();
		$this->load->view('frontpage/partials/wrapper' , $data);
	}

	// Add support
	public function add_support()
	{
		$data['contents']  = "frontpage/add_support";
		$data['url']       = uri_string();
		$this->load->view('frontpage/partials/wrapper' , $data);
	}

	// Detail support
	public function detail_support()
	{
		$data['contents']  = "frontpage/detail_support";
		$data['url']       = uri_string();
		$this->load->view('frontpage/partials/wrapper' , $data);
	}

	// -------------------
	// User
	public function login()
	{
		$this->load->view('frontpage/login');
	}
	
	// Register
	public function register()
	{
		$this->load->view('frontpage/register_user');
	}

	// Forgot password
	public function forgot_password()
	{
		$this->load->view('frontpage/forgot_password');
	}

}

/* End of file Frontpage.php */
/* Location: ./application/controllers/Frontpage.php */