<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ustadz extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (! $this->is_logged_in()) {
            show_404();
        }

        $this->load->helper(
            'form'
        );

        primary_navigation();
    }

    /**
     * Ustadz Home Page
     */
    public function index()
    {
        $ustadz = '<table><tr><th>Nama</th><th>Pendidikan Terakhir</th><th>Jabatan</th><th>Status</th><th>Action</th></tr>';
        $format = '<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>';

        $query = $this->db->get('ustadz');
        if ($result = $query->result()) {
            foreach ((array) $result as $item) {
                $ustadz .= sprintf(
                    $format, $item->ustadz_name, $item->pendidikan, $item->jabatan,
                    ($item->status == 0) ? 'Unverified' : 'Verified',
                    anchor(site_url('ustadz/edit/' . $item->ustadz_id), 'edit')
                );
            }
        }

        echo $ustadz . '</table>';
        echo "<br/>";
        echo anchor(
            site_url('ustadz/add-ustadz', $this->use_ssl),
            'Add New Ustad'
        );
    }

    /**
     * get kajian under ustadz
     * @param null $ustadz_id
     */
    public function kajian($ustadz_id = null)
    {
        if ( $ustadz_id == null ) {
            show_404();
        }

        $kajian = $this->general->get_kajian([
           'ustadz' => $ustadz_id,
            'date'  => [
                'start' => date('Y-m-d')
            ]
        ]);

        if ($kajian) {
            print_r($kajian);
        } else {
            echo 'No Result';
        }
    }
    /**
     * get ustadz by id
     * @param null $id
     */
    public function id($id = null)
    {
        if ($id == null) {
            show_404();
        }

        $query = $this->general->get_ustadz([
            'select' => 'id_ustadz, ustadz_name',
            'ustadz_id'   => $id
        ]);

        print_r($query);
        echo anchor(site_url('ustadz/kajian/' . $id, $this->use_ssl), 'Jadwal Kajian');
    }

    /**
     * HTML FORM Add Ustadz
     */
    public function add_ustadz()
    {
        $view_data['add_new_ustadz'] = site_url('ustadz/save-ustadz', $this->use_ssl);
        $this->load->view('ustadz/ustadz_form', $view_data);
    }

    /**
     * Handle Action Save
     */
    public function save_ustadz()
    {
        $this->load->library('form_validation');

        $ustadz_data = [
            'ustadz_name' => $this->input->post('name_ustadz'),
            'pendidikan'  => $this->input->post('study_ustadz'),
            'jabatan'     => $this->input->post('job_ustadz')
        ];

        $this->form_validation->set_data( $ustadz_data );

        $validations = [
            [
                'field' => 'ustadz_name', // field (key)
                'label' => 'Nama Ustadz', // input
                'rules' => 'required|trim|is_unique[ustadz.ustadz_name]',
                'errors' => [
                    'required'  => 'Nama Ustadz Wajib diisi',
                    'is_unique' => 'Nama Ustadz Sudah diinput'
                ]
            ],
            [
                'field' => 'pendidikan', // field (key)
                'label' => 'Pendidikan Terakhir Ustadz', // input
                'rules' => 'required|trim',
                'errors' => [
                    'required' => 'Pendidikan Terakhir Ustadz Wajib diisi'
                ]
            ],
            [
                'field' => 'jabatan', // field (key)
                'label' => 'jabatan Ustadz', // input
                'rules' => 'required|trim',
                'errors' => [
                    'required' => 'jabatan Ustadz Wajib diisi'
                ]
            ],
        ];

        $this->form_validation->set_rules( $validations );

        if ( $this->form_validation->run() ) {
            $insert = $this->db->set($ustadz_data)
                ->insert('ustadz');

            if ($insert) {
                echo '<h1>Selamat Ustadz '. $ustadz_data['ustadz_name'] .' telah berhasil ditambahkan</h1>';
                echo sprintf('<p>%s</p>',
                        anchor(site_url('ustadz/add-ustadz', $this->use_ssl), 'Kembali')
                    );
            }
        } else {
            echo '<h1>User Creation Error(s)</h1>' . validation_errors();
        }
    }

    /**
     * HTML Form Edit
     * @param null $id
     */
    public function edit($id = null)
    {
        if ($id == null) {
            show_404();
        }

        $result = $this->general->get_ustadz([
           'ustadz_id' => $id,
           'status'   => null
        ]);

        $view_data['actions'] = site_url('ustadz/update/' . $id, $this->use_ssl);
        $view_data['results'] = $result;

        $this->load->view('ustadz/ustadz_edit', $view_data);
    }

    /**
     * Handle Action Save
     * @param null $id
     */
    public function update($id = null)
    {
        if ($id == null) {
            show_404();
        }

        $this->load->library('form_validation');

        $ustadz_data = [
            'ustadz_name' => $this->input->post('name_ustadz'),
            'pendidikan'  => $this->input->post('study_ustadz'),
            'jabatan'     => $this->input->post('job_ustadz')
        ];

        $this->form_validation->set_data( $ustadz_data );

        $validations = [
            [
                'field' => 'ustadz_name', // field (key)
                'label' => 'Nama Ustadz', // input
                'rules' => 'required|trim',
                'errors' => [
                    'required'  => 'Nama Ustadz Wajib diisi',
                ]
            ],
            [
                'field' => 'pendidikan', // field (key)
                'label' => 'Pendidikan Terakhir Ustadz', // input
                'rules' => 'required|trim',
                'errors' => [
                    'required' => 'Pendidikan Terakhir Ustadz Wajib diisi'
                ]
            ],
            [
                'field' => 'jabatan', // field (key)
                'label' => 'jabatan Ustadz', // input
                'rules' => 'required|trim',
                'errors' => [
                    'required' => 'jabatan Ustadz Wajib diisi'
                ]
            ],
        ];

        $this->form_validation->set_rules( $validations );

        if ( $this->form_validation->run() ) {
            $this->db->where('id_ustadz', $id);
            $update = $this->db->update('ustadz', $ustadz_data);
            if ($update) {
                redirect(site_url('ustadz/edit/' . $id .'/?update=success', $this->use_ssl));
            }
        } else {
            $result = $this->general->get_ustadz([
                'ustadz_id' => $id,
                'status'   => null
            ]);

            $view_data['actions'] = site_url('ustadz/update', $this->use_ssl);
            $view_data['results'] = $result;

            $this->load->view('ustadz/ustadz_edit', $view_data);
        }
    }

    /**
     * Ajax Handler
     * get ustadz Name
     */
    public function get_ustadz()
    {
        if (! $this->input->is_ajax_request()) {
            show_404();
        }
        $keyword = $this->input->get('keyword');

        $query = $this->general->get_ustadz([
            'select' => 'id_ustadz, ustadz_name',
            'name'   => $keyword
        ]);

        $result = [
            'error' => 'Not Found',
            'keyword' => $keyword
        ];

        if ($query->result()) {
            $result = $query->result();
        }

        die(json_encode($result, JSON_PRETTY_PRINT));
    }

}