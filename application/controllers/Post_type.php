<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Post_type extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        if (
            strpos($this->uri->uri_string(), 'post_type') !== false ||
            strpos($this->uri->uri_string(), 'post-type') !== false
        ) {
            show_404();
        }

        $this->load->model('post_model');
    }

    public function get_post($slug = '')
    {
        if ( empty($slug) ) {
            echo 'blogpost';
        } else {
            echo $slug;
        }
    }

    public function get_page($slug = '')
    {
        if ( empty($slug) ) {
            show_404();
        }

        if ( $slug == 'tos' ) {
            $tos = $this->post_model->get_tos();
            echo $tos[0]->post_content;
        }
    }

    /**
     * Posts Handle
     */
    private function add_new_post()
    {}

    private function edit_post()
    {}

    private function save_post()
    {}

    /**
     * Pages Handle
     */
    private function add_new_page()
    {}

    private function edit_page()
    {}

    private function save_page()
    {}

}