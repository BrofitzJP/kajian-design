<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kajian extends MY_Controller
{
    private $view_data;

    public function __construct()
    {
        parent::__construct();

        if (! $this->is_logged_in()) {
            to_login();
        }

        $this->load->helper('form');

        $this->view_data = [
            'propinsi' => $this->general->get_propinsi(),
            'ssl'   => $this->use_ssl,
            'error'  => false,
            'error_message' => '',
            'search' => site_url('kajian/s', $this->use_ssl),
        ];
    }

    /**
     * Home Page Kajian
     */
    public function index()
    {
        $view_data = $this->view_data;

        // load pagination library
        $this->load->library('pagination');

        if ($this->general->get_count_kajian() > 0) {

            $query = $this->general->get_kajian();

            if ($query) {
                $view_data['error']  = false;
                $view_data['results'] = (array) $query;
            } else {
                $view_data['error'] = true;
                $view_data['error_message'] = 'No Kajian';
            }
        } else {
            $view_data['error'] = true;
            $view_data['error_message'] = 'No Kajian';
        }

        $view_data['add'] = anchor(
            site_url('kajian/add-new', $this->use_ssl),
            'Add New'
        );

        $this->load->view('kajian/kajian', $view_data);
    }

    /**
     * Single Page
     * @param $id
     */
    public function p($id)
    {
        $result = $this->general->get_kajian_by_id($id);
        print_r($result);
    }

    /**
     * FIND Kajian
     * @internal param $keyword
     */
    public function s()
    {
        // optional parameter;
        $args['date']['start'] = date('Y-m-d');
        $args['date']['end']   = date('Y') . '-12-31';

        if ($this->input->get('start') && ! empty($this->input->get('start'))) {
            $args['date']['start'] = date('Y-m-d', strtotime($this->input->get('start')));
        }

        if ($this->input->get('end') && ! empty($this->input->get('end'))) {
            $args['date']['end']  = date('Y-m-d', strtotime($this->input->get('end')));
        }

        // keyword
        $args['s'] = $this->input->get('s');

        /**
         * FILTER
         */
        if ($this->input->get('pro')) {
            $args['location']['provinces'] = $this->input->get('pro');

            if ($kabkota = $this->input->get('kabkota')) {
                $args['location']['regencies'] = $kabkota;

                if ($kecamatan = $this->input->get('kec')) {
                    $args['location']['districts'] = $kecamatan;

                    if ($kelurahan = $this->input->get('kel')) {
                        $args['location']['villages'] = $kelurahan;
                    }
                }
            }

        }

        // masjid
        if ($masjid = $this->input->get('mosq')) {
            $args['location']['mosque'] = $masjid;
        }

        // ustadz
        if ($ustadz = $this->input->get('ust')) {
            $args['ustadz'] = $ustadz;
        }

        // hari
        if ($day = $this->input->get('d')) {
            $args['days'] = $day;
        }

        $args['posts_per_page'] = 10;
        $args['page'] = $this->input->get('paged') ? : 0;

        // get Meta Filter Options
        $view_data['filter'] = $this->general->get_filter_kajian($args);
        $view_data['results'] = $this->general->get_kajian($args);

        $this->load->view('kajian/result', $view_data);
    }

    /**
     * HTML form add new kajian
     */
    public function add_new()
    {
        $ustadz = $this->db->get('ustadz');
        $mosque = $this->db->get('mosque');

        $view_data['ustadz']  = $ustadz->result();
        $view_data['masjid']  = $mosque->result();
        $view_data['actions'] = site_url('kajian/save', $this->use_ssl);

        $this->load->view('kajian/kajian_form', $view_data);
    }

    /**
     * Handle Action Save Kajian
     */
    public function save()
    {
        $user_id = $this->auth_user_id;
        $this->load->library('form_validation');

        $user_data = [
            'tema'       => $this->input->post('tema'),
            'ustadz_id'  => $this->input->post('ustadz'),
            'location'   => $this->input->post('masjid'),
            'date_time'  => $this->input->post('date'),
            'begin_time' => $this->input->post('begin'),
            'end_time'   => $this->input->post('end')
        ];

        $this->form_validation->set_data( $user_data );

        $validations = [
            [
                'field' => 'tema', // field (key)
                'label' => 'Tema Kajian', // input
                'rules' => 'required|trim',
                'errors' => [
                    'required' => 'Tema Kajian Wajib diisi'
                ]
            ],
            [
                'field' => 'ustadz_id', // field (key)
                'label' => 'Nama Ustads', // input
                'rules' => 'required|trim',
                'errors' => [
                    'required' => 'Nama Ustadz Wajib diisi'
                ]
            ],
            [
                'field' => 'location', // field (key)
                'label' => 'Nama Masjid', // input
                'rules' => 'required',
                'errors' => [
                    'required' => 'Nama Masjid Wajib diisi'
                ]
            ],
            [
                'field' => 'date_time', // field (key)
                'label' => 'Hari Dan Tanggal', // input
                'rules' => 'required',
                'errors' => [
                    'required' => 'Hari Dan Tanggal Wajib diisi'
                ]
            ],
            [
                'field' => 'begin_time', // field (key)
                'label' => 'Jam Mulai', // input
                'rules' => 'required',
                'errors' => [
                    'required' => 'Jam Mulai Wajib diisi'
                ]
            ],
            [
                'field' => 'end_time', // field (key)
                'label' => 'Jam Selesai', // input
                'rules' => 'required',
                'errors' => [
                    'required' => 'Jam Selsai Wajib diisi'
                ]
            ],
        ];

        $this->form_validation->set_rules( $validations );
        if ( $this->form_validation->run() ) {
            $user_data['date_create'] = date('Y-m-d H:i:s');
            $user_data['user_id'] = $user_id;
            $user_data['status']  = '1';
            $user_data['day']     = date('D', strtotime($user_data['date_time']));

            $insert = $this->db->set($user_data)
                ->insert('kajian');

            if ($insert) {
                redirect(site_url('kajian/?action=success', $this->use_ssl));
            } else {
                show_error($this->db->error(), 401, 'Insert GAGAL');
            }
        } else {
            $this->load->view('kajian/kajian_form');
        }

        $this->load->view('kajian/kajian', $view_data);
    }

    /**
     * HTML form edit kajian
     * @param $id
     */
    public function edit($id = null)
    {
        if ($id == null) {
            show_404();
        }

        $view_data = $this->view_data;
        if ( ! $this->this_is_my_post($id) ) {
            $view_data['error'] = 'cannot edit';
        } else {
            $view_data['actions'] = site_url('kajian/update/'. $id, $this->use_ssl);
            $view_data['result'] = $this->general->get_kajian_by_id($id);
        }

        $this->load->view('kajian/kajian_edit', $view_data);
    }

    /**
     *
     */
    public function update($id)
    {
        $view_data = [];
        $kajian_id = $this->input->post('kajian_id');
        if ( ! $this->this_is_my_post($kajian_id) ) {
            show_error('Cannot Access', 401, 'ERROR PERMISSION');
        } else {

            $user_id = $this->auth_user_id;
            $this->load->library('form_validation');

            $user_data = [
                'tema'       => $this->input->post('tema'),
                'ustadz_id'  => $this->input->post('ustadz_id'),
                'location'   => $this->input->post('masjid_id'),
                'date_time'  => $this->input->post('date'),
                'begin_time' => $this->input->post('begin'),
                'end_time'   => $this->input->post('end')
            ];

            $this->form_validation->set_data( $user_data );

            $validations = [
                [
                    'field' => 'tema', // field (key)
                    'label' => 'Tema Kajian', // input
                    'rules' => 'required|trim',
                    'errors' => [
                        'required' => 'Tema Kajian Wajib diisi'
                    ]
                ],
                [
                    'field' => 'ustadz_id', // field (key)
                    'label' => 'Nama Ustads', // input
                    'rules' => 'required|trim',
                    'errors' => [
                        'required' => 'Nama Ustadz Wajib diisi'
                    ]
                ],
                [
                    'field' => 'location', // field (key)
                    'label' => 'Nama Masjid', // input
                    'rules' => 'required',
                    'errors' => [
                        'required' => 'Nama Masjid Wajib diisi'
                    ]
                ],
                [
                    'field' => 'date_time', // field (key)
                    'label' => 'Hari Dan Tanggal', // input
                    'rules' => 'required',
                    'errors' => [
                        'required' => 'Hari Dan Tanggal Wajib diisi'
                    ]
                ],
                [
                    'field' => 'begin_time', // field (key)
                    'label' => 'Jam Mulai', // input
                    'rules' => 'required',
                    'errors' => [
                        'required' => 'Jam Mulai Wajib diisi'
                    ]
                ],
                [
                    'field' => 'end_time', // field (key)
                    'label' => 'Jam Selesai', // input
                    'rules' => 'required',
                    'errors' => [
                        'required' => 'Jam Selsai Wajib diisi'
                    ]
                ],
            ];

            $this->form_validation->set_rules( $validations );
            if ( $this->form_validation->run() ) {
                $user_data['day'] = date('D', strtotime($user_data['date_time']));

                $this->db->where('kajian_id', $kajian_id);

                $update = $this->db->update('kajian', $user_data);

                if ($update) {
                    redirect(site_url('kajian/edit/' . $kajian_id .'/?update=success', $this->use_ssl));
                } else {
                    $this->db->error();
                    show_error($this->db->error(), 401, 'Insert GAGAL');
                }
            } else {
                $this->load->view('kajian/kajian_edit');
            }
        }
    }

    /**
     * set post/kajian to draft/unpublish
     * @param $kajian_id
     */
    public function drafting($kajian_id)
    {
        $view_data = [];
        if ( ! $this->this_is_my_post($kajian_id) ) {
            show_error('Cannot Access', 401, 'ERROR PERMISSION');
        } else {
            $this->db->where('kajian_id', $kajian_id);
            $this->db->update('kajian', [
               'status' => '0'
            ]);

            redirect(site_url('kajian/p/' . $kajian_id, $this->use_ssl));
        }
    }

    /**
     * as you know :v
     * @param $kajian_id
     */
    public function delete($kajian_id)
    {
        $view_data = [];
        if ( ! $this->this_is_my_post($kajian_id) ) {
            show_error('Cannot Access', 401, 'ERROR PERMISSION');
        } else {
            $this->db->delete('kajian', ['kajian_id' => $kajian_id]);
            redirect(site_url('kajian', $this->use_ssl));
        }

    }

    /**
     * check your is owner or not
     * @param $kajian_id
     * @return bool
     */
    private function this_is_my_post($kajian_id)
    {
        if ($this->auth_user_id) {
            $user_id = $this->auth_user_id;
        }
        if (! isset($user_id)) {
            return false;
        }

        $query = $this->db->get_where('kajian', [
            'kajian_id' => $kajian_id,
            'user_id'   => $user_id
        ]);

        if ($query->num_rows() > 0) {
            return true;
        }

        return false;
    }

}