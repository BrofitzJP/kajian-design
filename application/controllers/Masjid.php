<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Class Masjid
 * @property CI_Upload $upload
 */
class Masjid extends MY_Controller
{
    private $view_data;

    public function __construct()
    {
        parent::__construct();
        // harus login
        if (! $this->is_logged_in()) {
            to_login();
        }

        $this->load->helper(
            'form'
        );

        $this->view_data = [
            'use_ssl' => $this->use_ssl
        ];
    }

    /**
     * Home Masjid
     */
    public function index()
    {
        $masjid = $this->general->get_masjid([
            'wheres' => [
                'field' => 'page'
            ]
        ]);

        $view_data = $this->view_data;

        $view_data['masjid'] = $masjid;
        $view_data['add'] = anchor(site_url('masjid/add', $this->use_ssl), 'Add Masjid');
        $this->load->view('masjid/masjid', $view_data);
    }

    /**
     * Masjid Detail Information
     * @param $mosque_id
     * @url /masjid/d/(:num)
     */
    public function detail($mosque_id = null)
    {
        if ( (strpos($this->uri->uri_string(), 'masjid/detail') !== false) ||
             $mosque_id == null
        ) {
            show_404();
        }

        /**
         * Get Masjid detail information
         */
        $masjid = $this->general->get_masjid_data($mosque_id);
        $view_data['masjid'] = $masjid[0];

        print_r($masjid);
        echo $mosque_id . ' is exist';
    }

    /**
     * Form to Add New Masjid
     */
    public function add()
    {
        $view_data = [
            'use_ssl' => $this->use_ssl,
            'actions' => site_url('masjid/save', $this->use_ssl),
            'provinces' => $this->general->get_propinsi()
        ];

        $this->load->view('masjid/masjid_form', $view_data);
    }

    /**
     * HTML Form to Add Editor Masjid
     * @param $masjid_id
     */
    public function add_admin($masjid_id = null)
    {
        if ($masjid_id == null) {
            show_404();
        }

        // check is Admin or Not
        $role = $this->general->get_masjid_role($this->auth_user_id, $masjid_id);
        if ( $role == '9') {
            $masjid = $this->general->get_masjid_data($masjid_id);

            if ( isset($masjid[0]) ) {
                $view_data['role']   = $role;
                $view_data['masjid'] = $masjid[0];
                $view_data['action'] = site_url('masjid/save-admin', $this->use_ssl);
                $view_data['free_users'] = $this->get_user_isnot_manager_in_mosque($masjid_id);

//                print_r($view_data);
                $this->load->view('masjid/masjid_manager_form', $view_data);
            }
        } else {
            show_error('Cannot Access', 401, 'ERROR PERMISSION');
        }
    }

    /**
     * Saving Masjid & Admin Masjid Data
     */
    public function save()
    {
        $this->load->library('form_validation');

        $config = [
            'upload_path'  => FCPATH . '/media/attachment/',
            'allowed_types' => 'gif|jpg|png',
            'file_ext_tolower' => true,
        ];

        $this->load->library('upload', $config);
        
        $image_name = '';

        if ( ! $this->upload->do_upload('photo_masjid')) {
            $error = array('error' => $this->upload->display_errors());

            print_r($error); exit();
        } else {
            $image = $this->upload->data();
            $image_name = $image['file_name'];
        }

        $validation = [
            [
                'field' => 'name_masjid',
                'label' => 'masjid',
                'rules' => 'required|trim',
                'errors' => [
                    'required' => 'Nama Masjid Harus diisi'
                ]
            ],
            [
                'field' => 'address',
                'label' => 'address',
                'rules' => 'required|trim',
                'errors' => [
                    'required' => 'Alamat Harus diisi'
                ]
            ],

        ];

        $this->form_validation->set_rules( $validation );
        if ( $this->form_validation->run() ) {

            $location = '';
            if ( $this->input->post('propinsi') ) {
                $location = $this->input->post('propinsi');
                if ($this->input->post('kabkota')) {
                    $location = $this->input->post('kabkota');
                    if ($this->input->post('kec')) {
                        $location = $this->input->post('kec');
                        if ($this->input->post('kelurahan')) {
                            $location = $this->input->post('kelurahan');
                        }
                    }
                }
            }

            $location = trim($location);

            $masjid_data = [
                'user_creator_id' => $this->auth_user_id,
                'date_create' => date('Y-m-d H:i:s'),
                'mosque_name' => $this->input->post('name_masjid'),
                'address' => $this->input->post('address'),
                'location' => $location,
                'takmir' => $this->input->post('takmir'),
                'telp_takmir' => $this->input->post('no_takmir'),
                'telp_takmir_other' => $this->input->post('no_takmir_ii'),
                'email' => $this->input->post('email'),
                'jamaah' => $this->input->post('jumlah'),
                'maps' => htmlentities($this->input->post('map')),
                'attachment' => $image_name
            ];

            $insert = $this->db->set($masjid_data)->insert('mosque');

            if ($insert) {
                $mosque_id = $this->db->insert_id();

                $manager = $this->add_manager($mosque_id, $this->auth_user_id, '9');

                if ( ! $manager) {
                    die($this->db->error());
                }

                redirect(site_url('masjid/add-admin/' . $mosque_id, $this->use_ssl));
            }
        } else {
            echo '<h1>User Creation Error(s)</h1>' . validation_errors();
        }
    }

    /**
     * Saving Masjid Editor
     */
    public function save_admin()
    {
        $mosque_id = $this->input->post('mosque_id');
        if ( $this->general->get_masjid_role($this->auth_user_id, $mosque_id) == '9') {
            $role = $this->input->post('role');
            $editor = array_map("unserialize", array_unique(array_map("serialize", $this->input->post('editor'))));

            $editor_to_mosque = 'INSERT INTO mosque_manager (mosque_id, user_id, role) VALUES ';

            foreach ($editor as $user_id) {
                if (!$this->general->have_manager($mosque_id, $user_id) && $user_id) {
                    $editor_to_mosque .= '( ' . $this->db->escape($mosque_id) . ', ';
                    $editor_to_mosque .= $this->db->escape($user_id) . ', ';
                    $editor_to_mosque .= ($role ? $role : '1') . ' ), ';
                }
            }
            $insert = $this->db->query(trim($editor_to_mosque, ', '));

            if ($insert) {
                redirect(site_url('masjid/d/' . $mosque_id, $this->use_ssl));
            } else {
                show_error($this->db->error(), 200, 'Insert GAGAL');
            }
        } else {
            show_error('Cannot Access', 200, 'ERROR PERMISSION');
        }
    }

    /**
     * FORM Edit Masjid
     * @param $masjid_id
     */
    public function edit($masjid_id = null)
    {
        if ($masjid_id == null) {
            show_404();
        }

        $role = $this->general->get_masjid_role($this->auth_user_id, $masjid_id);

        if ($role !== false) {
            $view_data['use_ssl'] = $this->use_ssl;
            $view_data['admin_role'] = $role;
            $view_data['actions'] = site_url('masjid/update/' . $masjid_id, $this->use_ssl);
            $view_data['provinces'] = $this->general->get_propinsi();
            $view_data['masjid']  = $this->general->get_masjid_data($masjid_id);
            $this->load->view('masjid/masjid_edit', $view_data);
        } else {
            show_error('Cannot Access', 200, 'ERROR PERMISSION');
        }
    }

    /**
     * Handle Action Update Masjid + Editor
     */
    public function update($masjid_id)
    {
        $this->load->library('form_validation');

        $config = [
            'upload_path'  => FCPATH . '/media/attachment/',
            'allowed_types' => 'gif|jpg|png',
            'file_ext_tolower' => true,
        ];

        $this->load->library('upload', $config);
        $upload = $_FILES['photo_masjid']['error'];
        $image_name = '';
        if ( $upload == 0 && $this->upload->do_upload('photo_masjid')) {

            $image = $this->upload->data();
            $image_name = $image['file_name'];
        }

        $validation = [
            [
                'field' => 'name_masjid',
                'label' => 'masjid',
                'rules' => 'required|trim',
                'errors' => [
                    'required' => 'Nama Masjid Harus diisi'
                ]
            ],
            [
                'field' => 'address',
                'label' => 'address',
                'rules' => 'required|trim',
                'errors' => [
                    'required' => 'Alamat Harus diisi'
                ]
            ],

        ];

        $this->form_validation->set_rules( $validation );
        if ( $this->form_validation->run() ) {

            $location = '';
            if ( $this->input->post('propinsi') ) {
                $location = $this->input->post('propinsi');
                if ($this->input->post('kabkota')) {
                    $location = $this->input->post('kabkota');
                    if ($this->input->post('kec')) {
                        $location = $this->input->post('kec');
                        if ($this->input->post('kelurahan')) {
                            $location = $this->input->post('kelurahan');
                        }
                    }
                }
            }

            $location = trim($location);

            $masjid_data = [
                'mosque_name' => $this->input->post('name_masjid'),
                'address' => $this->input->post('address'),
                'location' => $location,
                'takmir' => $this->input->post('takmir'),
                'telp_takmir' => $this->input->post('no_takmir'),
                'telp_takmir_other' => $this->input->post('no_takmir_ii'),
                'email' => $this->input->post('email'),
                'jamaah' => $this->input->post('jumlah'),
                'maps' => htmlentities($this->input->post('map')),
            ];

            if ($upload == 0) {
                $masjid_data['attachment'] = $image_name;
            }

            $this->db->where('mosque_id', $masjid_id);
            $update = $this->db->update('mosque', $masjid_data);

            if ($update) {
                redirect(site_url('masjid/edit/' . $masjid_id, $this->use_ssl));
            }
        } else {
            echo '<h1>User Creation Error(s)</h1>' . validation_errors();
        }
    }

    public function delete_manager($masjid_id, $editor_id)
    {
        $role = $this->general->get_masjid_role($this->auth_user_id, $masjid_id);
        if ($role !== '9') {
            show_error('Cannot Access', 200, 'ERROR PERMISSION');
        }

        $this->db->delete('mosque_manager', ['user_id' => $editor_id, 'mosque_id' => $masjid_id]);

        redirect(site_url('masjid/d/' . $masjid_id, $this->use_ssl));
    }

    /**
     * AJAX HANDLER
     * to get kabupaten, kecamatan, dan kelurahan.
     */
    public function get_kabupaten()
    {
        if (! $this->input->is_ajax_request()) {
            show_404();
        }

        $propinsi_id = $this->input->get('propinsi_id');
//        $propinsi_id = 13;
        $kabupaten = $this->db->get_where('regencies', [
            'province_id' => $propinsi_id
        ]);

        $result = [
            'error' => 'Not Found'
        ];

        if ($kabupaten->result()) {
            $result = $kabupaten->result();
        }

        die(json_encode($result, JSON_PRETTY_PRINT));
    }

    public function get_kecamatan()
    {
        if (! $this->input->is_ajax_request()) {
            show_404();
        }

        $kabupaten_id= $this->input->get('kabupaten_id');

        $kecamatan = $this->db->get_where('districts', [
            'regency_id' => $kabupaten_id
        ]);

        $result = [
            'error' => 'Not Result'
        ];

        if ($kecamatan->result()) {
            $result = $kecamatan->result();
        }

        die(json_encode($result, JSON_PRETTY_PRINT));
    }

    public function get_kelurahan()
    {
        if (! $this->input->is_ajax_request()) {
            show_404();
        }

        $kecamatan_id= $this->input->get('kecamatan_id');

        $kelurahan = $this->db->get_where('villages', [
            'district_id' => $kecamatan_id
        ]);

        $result = [
            'error' => 'Not Result'
        ];

        if ($kelurahan->result()) {
            $result = $kelurahan->result();
        }

        die(json_encode($result, JSON_PRETTY_PRINT));
    }

    /**
     * AJAX Search Get Masjid
     */
    public function get_masjidjson()
    {
        if (! $this->input->is_ajax_request()) {
            show_404();
        }

        $keyword = $this->input->get('keyword');

        $query   = $this->general->get_masjid_location([
            'find' => $keyword
        ]);

        $result = [
            'error' => 'Not Found'
        ];

        if ($query) {
            $result = $query;
        }

        die(json_encode($result, JSON_PRETTY_PRINT));
    }

    /**
     * get kalurahan for input suggest
     */
    public function get_kelurahanjson()
    {
        if ( !$this->input->is_ajax_request() ) {
            show_404();
        }

        $keyword = $this->input->get('keyword');

        $cari = $this->general->get_lokasi([
                    'find' => [
                        'field' => 'vil.name', // field ready name, id | as table pro, reg, dis, and vil
                        'value' => $keyword,    // content of name column
                        'operator' => 'like' // or not_like
                    ],
                    'wheres' => [], // multidimesi array
                    'limit'  => 20
                ]);
        $result = [
            'error' => 'Oops! No Result'
        ];
        if ($cari) {
            $result = $cari;
        }

        die(json_encode($result, JSON_PRETTY_PRINT));
    }

    /**
     * @param $masjid_id
     * @param $user_id
     * @param $role
     * @return bool
     */
    private function add_manager($mosque_id, $user_id, $role = '1')
    {
        $insert_manager = $this->db->insert('mosque_manager', [
            'mosque_id' => $mosque_id,
            'user_id'   => $user_id,
            'role'      => $role
        ]);

        return $insert_manager;
    }

    /**
     * @param string 1 and 9, 1 == Editor, 9 == Admin
     */
    private function update_role_mosque($role, $user_id, $mosque_id)
    {
        $where = [
            'user_id' => $user_id,
            'mosque_id' => $mosque_id
        ];

        $this->db->where($where)->update('mosque_manager', [
            'role' => $role
        ]);
    }


    /**
     * get users is not manager (admin or editor) in current mosque/Masjid
     * @param $mosque_id
     * @return bool
     */
    private function get_user_isnot_manager_in_mosque($mosque_id)
    {
        $sql = "SELECT u.user_id, u.display_name 
                FROM users u 
                WHERE NOT EXISTS (
                  SELECT * FROM mosque_manager mm 
                  WHERE mm.user_id = u.user_id AND mm.mosque_id = $mosque_id
                )";

        $users = $this->db->query($sql);
        if ($users->num_rows() > 0) {
            return $users->result();
        }

        return false;
    }
}