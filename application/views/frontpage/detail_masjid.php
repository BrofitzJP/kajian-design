<div class="top-page">
	<div class="title-page">Detail Masjid</div>
</div>

<!-- Content Page -->
<div class="container app-content">
	<div class="row">
		<div class="col s12">
			<div class="card">
				<div class="card-image">
					<img src="<?php echo site_url() ?>/assets/images/mosque.jpg">
					<span class="card-title">Masjid Al-Aqsa</span>
					<a href="<?php echo site_url() ?>frontpage/edit_masjid" class="btn-floating btn-large halfway-fab waves-effect waves-light red"><i class="material-icons">mode_edit</i></a>
				</div>
				<div class="card-content p-t-30">
					<p><strong>Alamat</strong> : Mojogedang RT03/R02, Kec. Mojogedang, Kab. Karanganyar, Prov. Jawa Tengah</p>
					<p><strong>Takmir</strong> : Brofit J</p>
					<p><strong>Kontak Takmir I</strong> : 0822-4285-0853</p>
					<p><strong>Kontak Takmir II</strong> : 0822-4285-0855</p>
					<p><strong>Email</strong> : <a href="mailto:brofitzjp@gmail.com">brofitzjp@gmail.com</a></p>
					<p><strong>Jumlah Jamaah Subuh</strong> : 230</p>
				</div>
			</div>
			<div class="gmap-mosque">
				<p class="m-b-10"><em>Cari masjid ini dengan google map</em></p>
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.714885493507!2d106.83015131431304!3d-6.168919995533785!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f5ce56ecf48f%3A0x43d85d138825d3b6!2sMasjid+Istiqlal%2C+Jl.+Kathedral!5e0!3m2!1sen!2sid!4v1515376876155" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
			<!-- Kajian -->
			<p><em>Terdapat 2 kajian pada masjid Al-Aqsa</em></p>
			<div class="card kajian-card">
				<span class="title-card">Menuju Keluarga Bahagia Dunia Akhirat</span>
				<div class="card-image waves-effect waves-block waves-light">
					<img class="activator" src="<?php echo site_url('') ?>/assets/images/tree.jpg">
				</div>
				<div class="card-content">
					<div class="float-inner-card">
						<div class="people-img">
							<img src="<?php echo site_url('') ?>/assets/images/abdul_somad.jpg" alt="">
						</div>
						<div class="top-inner-card">
							<span class="utz-card">Abdul Somad Lc. MA</span>
						</div>
						<div class="bottom-inner-card">
							<span><i class="material-icons">date_range</i> Minggu, 02 Jan 2018</span>
							<span><i class="material-icons">alarm</i> 08.00 - 10.00 WIB</span>
						</div>
					</div>
					<div class="loc-card">
						<span><i class="material-icons">brightness_4</i> Masjid Al-Ikhlas</span>
						<span><i class="material-icons">place</i> Mojogedang RT03/RW 02 Kec. Mojogedang, Kab. Karanganyar, Prov. Jawa Tengah</span>
					</div>
				</div>
				<div class="card-footer">
					<span><i class="material-icons">account_circle</i> By Brofit J</span>
					<span class="activator right">Selengkapnya <i class="material-icons">keyboard_arrow_up</i></span>
				</div>
				<div class="card-reveal">
					<span class="card-title grey-text text-darken-4">Menuju Keluarga Bahagia Dunia Akhirat<i class="material-icons right">close</i></span>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
					<div class="footer-reveal">
						<em>Download file kajian</em>
						<button class="btn waves-effect waves-light">Download
							<i class="material-icons right">file_download</i>
						</button>
					</div>
				</div>
			</div>
			<div class="card kajian-card">
				<span class="title-card">Menuju Keluarga Bahagia Dunia Akhirat</span>
				<div class="card-image waves-effect waves-block waves-light">
					<img class="activator" src="<?php echo site_url('') ?>/assets/images/tree.jpg">
				</div>
				<div class="card-content">
					<div class="float-inner-card">
						<div class="people-img">
							<img src="<?php echo site_url('') ?>/assets/images/abdul_somad.jpg" alt="">
						</div>
						<div class="top-inner-card">
							<span class="utz-card">Abdul Somad Lc. MA</span>
						</div>
						<div class="bottom-inner-card">
							<span><i class="material-icons">date_range</i> Minggu, 02 Jan 2018</span>
							<span><i class="material-icons">alarm</i> 08.00 - 10.00 WIB</span>
						</div>
					</div>
					<div class="loc-card">
						<span><i class="material-icons">brightness_4</i> Masjid Al-Ikhlas</span>
						<span><i class="material-icons">place</i> Mojogedang RT03/RW 02 Kec. Mojogedang, Kab. Karanganyar, Prov. Jawa Tengah</span>
					</div>
				</div>
				<div class="card-footer">
					<span><i class="material-icons">account_circle</i> By Brofit J</span>
					<span class="activator right">Selengkapnya <i class="material-icons">keyboard_arrow_up</i></span>
				</div>
				<div class="card-reveal">
					<span class="card-title grey-text text-darken-4">Menuju Keluarga Bahagia Dunia Akhirat<i class="material-icons right">close</i></span>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
					<div class="footer-reveal">
						<em>Download file kajian</em>
						<button class="btn waves-effect waves-light">Download
							<i class="material-icons right">file_download</i>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>