<div class="top-page">
	<div class="title-page">Detail Ustadz</div>
</div>

<!-- Content Page -->
<div class="container app-content">
	<div class="row">
		<div class="col s12">
			<!-- Card -->
			<div class="card kajian-card utz-detail">
				<img class="uts-image" src="<?php echo site_url('') ?>assets/images/abdul_somad_300x300.jpg" alt="">
				<div class="card-content">
					<p class="utz">Abdul Somad Lc. MA</p>
					<p>Pendidikan Terakhir di Maroko</p>
					<p>Jabatan Pegawai Negeri Sipil</p>
					<p>Memiliki 4 Kajian</p>
					<p class="utz-verified">Verified</p>
				</div>
				<a href="<?php echo site_url() ?>/frontpage/edit_ustadz" class="btn-floating btn-large halfway-fab waves-effect waves-light red">
					<i class="material-icons">create</i>
				</a>
			</div>
				
			<!-- Kajian Ustadz -->
			<p class="suggested"><em>Kajian Ustad Abdul Somad Lc. MA</em></p>
			<div class="card kajian-card">
				<span class="title-card">Lorem Ipsum Dolor Sit Amet</span>
				<div class="card-image waves-effect waves-block waves-light">
					<img class="activator" src="<?php echo site_url('') ?>/assets/images/tree.jpg">
				</div>
				<div class="card-content">
					<div class="float-inner-card">
						<div class="people-img">
							<img src="<?php echo site_url('') ?>/assets/images/abdul_somad.jpg" alt="">
						</div>
						<div class="top-inner-card">
							<span class="utz-card">Abdul Somad Lc. MA</span>
						</div>
						<div class="bottom-inner-card">
							<span><i class="material-icons">date_range</i> Minggu, 02 Jan 2018</span>
							<span><i class="material-icons">alarm</i> 08.00 - 10.00 WIB</span>
						</div>
					</div>
					<div class="loc-card">
						<span><i class="material-icons">brightness_4</i> Masjid Al-Ikhlas</span>
						<span><i class="material-icons">place</i> Mojogedang, Karanganyar, Jawa Tengah</span>
					</div>
				</div>
				<div class="card-footer">
					<span><i class="material-icons">account_circle</i> By Brofit J</span>
					<span class="activator right">Selengkapnya <i class="material-icons">keyboard_arrow_up</i></span>
				</div>
				<div class="card-reveal">
					<span class="card-title grey-text text-darken-4">Lorem Ipsim Dolor Sit Amet<i class="material-icons right">close</i></span>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
				</div>
			</div>
			<div class="card kajian-card">
				<span class="title-card">Lorem Ipsum Dolor Sit Amet</span>
				<div class="card-image waves-effect waves-block waves-light">
					<img class="activator" src="<?php echo site_url('') ?>/assets/images/tree.jpg">
				</div>
				<div class="card-content">
					<div class="float-inner-card">
						<div class="people-img">
							<img src="<?php echo site_url('') ?>/assets/images/abdul_somad.jpg" alt="">
						</div>
						<div class="top-inner-card">
							<span class="utz-card">Abdul Somad Lc. MA</span>
						</div>
						<div class="bottom-inner-card">
							<span><i class="material-icons">date_range</i> Minggu, 02 Jan 2018</span>
							<span><i class="material-icons">alarm</i> 08.00 - 10.00 WIB</span>
						</div>
					</div>
					<div class="loc-card">
						<span><i class="material-icons">brightness_4</i> Masjid Al-Ikhlas</span>
						<span><i class="material-icons">place</i> Mojogedang, Karanganyar, Jawa Tengah</span>
					</div>
				</div>
				<div class="card-footer">
					<span><i class="material-icons">account_circle</i> By Brofit J</span>
					<span class="activator right">Selengkapnya <i class="material-icons">keyboard_arrow_up</i></span>
				</div>
				<div class="card-reveal">
					<span class="card-title grey-text text-darken-4">Lorem Ipsim Dolor Sit Amet<i class="material-icons right">close</i></span>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
				</div>
			</div>
			<div class="card kajian-card">
				<span class="title-card">Lorem Ipsum Dolor Sit Amet</span>
				<div class="card-image waves-effect waves-block waves-light">
					<img class="activator" src="<?php echo site_url('') ?>/assets/images/tree.jpg">
				</div>
				<div class="card-content">
					<div class="float-inner-card">
						<div class="people-img">
							<img src="<?php echo site_url('') ?>/assets/images/abdul_somad.jpg" alt="">
						</div>
						<div class="top-inner-card">
							<span class="utz-card">Abdul Somad Lc. MA</span>
						</div>
						<div class="bottom-inner-card">
							<span><i class="material-icons">date_range</i> Minggu, 02 Jan 2018</span>
							<span><i class="material-icons">alarm</i> 08.00 - 10.00 WIB</span>
						</div>
					</div>
					<div class="loc-card">
						<span><i class="material-icons">brightness_4</i> Masjid Al-Ikhlas</span>
						<span><i class="material-icons">place</i> Mojogedang, Karanganyar, Jawa Tengah</span>
					</div>
				</div>
				<div class="card-footer">
					<span><i class="material-icons">account_circle</i> By Brofit J</span>
					<span class="activator right">Selengkapnya <i class="material-icons">keyboard_arrow_up</i></span>
				</div>
				<div class="card-reveal">
					<span class="card-title grey-text text-darken-4">Lorem Ipsim Dolor Sit Amet<i class="material-icons right">close</i></span>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
				</div>
			</div>
			<div class="card kajian-card">
				<span class="title-card">Lorem Ipsum Dolor Sit Amet</span>
				<div class="card-image waves-effect waves-block waves-light">
					<img class="activator" src="<?php echo site_url('') ?>/assets/images/tree.jpg">
				</div>
				<div class="card-content">
					<div class="float-inner-card">
						<div class="people-img">
							<img src="<?php echo site_url('') ?>/assets/images/abdul_somad.jpg" alt="">
						</div>
						<div class="top-inner-card">
							<span class="utz-card">Abdul Somad Lc. MA</span>
						</div>
						<div class="bottom-inner-card">
							<span><i class="material-icons">date_range</i> Minggu, 02 Jan 2018</span>
							<span><i class="material-icons">alarm</i> 08.00 - 10.00 WIB</span>
						</div>
					</div>
					<div class="loc-card">
						<span><i class="material-icons">brightness_4</i> Masjid Al-Ikhlas</span>
						<span><i class="material-icons">place</i> Mojogedang, Karanganyar, Jawa Tengah</span>
					</div>
				</div>
				<div class="card-footer">
					<span><i class="material-icons">account_circle</i> By Brofit J</span>
					<span class="activator right">Selengkapnya <i class="material-icons">keyboard_arrow_up</i></span>
				</div>
				<div class="card-reveal">
					<span class="card-title grey-text text-darken-4">Lorem Ipsim Dolor Sit Amet<i class="material-icons right">close</i></span>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
				</div>
			</div>
		</div>
	</div>
</div>