<div class="title-page">Edit Masjid</div>

<!-- Content Page -->
<div class="container app-content">
	<div class="row">
		<div class="col s12">
			<ul class="collection app-coll-mosque">
				<li class="collection-item">
					<img src="<?php echo site_url('') ?>assets/images/mosque_300x300.jpg" alt="">
					<span class="title">Masjid Al-Ikhlas</span>
					<p style="padding-right: 0px"><span>Mojogedang RT03/RW02, Kec. Mojogedang, Kab. Mojogedang, Prov. Jawa Tengah</span></p>
				</li>
			</ul>
			<div class="card-panel app-card">
				<div class="row">
					<!-- Edit Majid -->
					<form class="col s12">
						<div class="row">
							<div class="input-field col s12">
								<i class="material-icons prefix">brightness_4</i>
								<input id="name-mosque" name="" type="text" value="Al-Ikhlas">
								<label for="name-mosque">Nama Masjid</label>
							</div>
							<div class="input-field col s12">
								<i class="material-icons prefix">location_on</i>
								<select class="select2">
									<option value="" disabled>Pilih Provinsi</option>
									<option value="1">Banten</option>
									<option value="2">Jakarta</option>
									<option value="3">Jawa Barat</option>
									<option value="4" selected>Jawa Tengah</option>
									<option value="5">Yogyakarta</option>
									<option value="6">Jawa Timur</option>
								</select>
								<label class="label-select2">Pilih Provinsi</label>
							</div>
							<div class="input-field col s12">
								<i class="material-icons prefix">location_on</i>
								<select class="select2">
									<option value="" disabled selected>Pilih Kabupaten</option>
									<option value="1" selected>Karanganyar</option>
									<option value="2">Jakarta</option>
									<option value="3">Jawa Barat</option>
									<option value="4">Jawa Tengah</option>
									<option value="5">Yogyakarta</option>
									<option value="6">Jawa Timur</option>
								</select>
								<label class="label-select2">Pilih Kabupaten</label>
							</div>
							<div class="input-field col s12">
								<i class="material-icons prefix">location_on</i>
								<select class="select2">
									<option value="" disabled selected>Pilih Kacamatan</option>
									<option value="1" selected>Mojogedang</option>
									<option value="2">Jakarta</option>
									<option value="3">Jawa Barat</option>
									<option value="4">Jawa Tengah</option>
									<option value="5">Yogyakarta</option>
									<option value="6">Jawa Timur</option>
								</select>
								<label class="label-select2">Pilih Kecamatan</label>
							</div>
							<div class="input-field col s12">
								<i class="material-icons prefix">my_location</i>
								<input id="address" name="" type="text" value="Mojogedang RT03/RW02">
								<label for="address">Alamat</label>
							</div>
							<div class="input-field col s12">
								<i class="material-icons prefix">person</i>
								<input id="name-takmir" name="" type="text" value="Brofit J">
								<label for="name-takmir">Nama Tekmir</label>
							</div>
							<div class="input-field col s12">
								<i class="material-icons prefix">phone</i>
								<input id="contact1" name="" type="text" value="0822-4285-0853">
								<label for="contact1">Kontak Tekmir</label>
							</div>
							<div class="input-field col s12">
								<i class="material-icons prefix">phone</i>
								<input id="contact2" name="" type="tel" value="0822-4285-0854">
								<label for="contact2">Kontak Tekmir II</label>
							</div>
							<div class="input-field col s12">
								<i class="material-icons prefix">email</i>
								<input id="email" name="" type="email" value="brofitzjp@gmail.com">
								<label for="email">Email</label>
							</div>
							<div class="input-field col s12">
								<i class="material-icons prefix">people</i>
								<input id="email" name="" type="email" value="320">
								<label for="email">Jumlah Jamaah Subuh</label>
							</div>
							<div class="input-field col s12">
								<i class="material-icons prefix">link</i>
								<input id="gmap" name="" type="url" value="http://google-map.com/123DF3">
								<label for="gmap">Link Google Map</label>
							</div>
							<div class="file-field input-field col s12">
								<div class="btn btn-small red">
									<i class="material-icons">attachment</i>
									<input type="file">
								</div>
								<div class="file-path-wrapper">
									<input class="file-path" type="text" placeholder="Upload foto masjid" value="foto-masjid.jpg">
								</div>
							</div>
						</div>
						<button class="btn waves-effect waves-light right" type="submit" name="action">Submit
							<i class="material-icons right">send</i>
						</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>