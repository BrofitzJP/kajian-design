<!doctype html>
<html lang="en">
	<head>
		<title>Kajian App</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- CSS -->
		<link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
		<link rel="stylesheet" href="<?php echo site_url('') ?>assets/frameworks/materialize/css/materialize.min.css" crossorigin="anonymous">
		<link rel="stylesheet" href="<?php echo site_url('') ?>assets/plugins/dropzone/dropzone.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="<?php echo site_url('') ?>assets/plugins/select2/select2.min.css">
        <link rel="stylesheet" href="<?php echo site_url('') ?>assets/plugins/select2/select2-materializecss.css">
		<link rel="stylesheet" href="<?php echo site_url('') ?>assets/css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		
		<?php  
			$this->load->view('frontpage/partials/header');
			$this->load->view('frontpage/partials/sidebar');
			?>
				<div id="test-swipe-1">
					<?php
						if ($contents) {
							$this->load->view($contents);
						}
					?>
				</div>
			<?php
			$this->load->view('frontpage/partials/nav-bottom');
		?>

		<!-- JavaScript -->
		<script src="<?php echo site_url() ?>assets/js/jquery.min.js" crossorigin="anonymous"></script>
		<script src="<?php echo site_url() ?>assets/plugins/dropzone/dropzone.js" crossorigin="anonymous"></script>
        <script src="<?php echo site_url() ?>assets/plugins/select2/select2.full.min.js" crossorigin="anonymous"></script>
		<script src="<?php echo site_url() ?>assets/frameworks/materialize/js/materialize.min.js" crossorigin="anonymous"></script>
		<script src="<?php echo site_url() ?>assets/js/front.js" crossorigin="anonymous"></script>
		<script>
			$(document).ready(function() {
				$("#ustadz").select2({
					tags: true
				});

				$("#btn-add-utz").on("click", function(){
					var newUtzVal = $("#new-utz").val();
					// Set the value, creating a new option if necessary
					if ($("#ustadz").find("option[value='" + newUtzVal + "']").length) {
						$("#ustadz").val(newUtzVal).trigger("change");
					} else { 
						// Create the DOM option that is pre-selected by default
						var newState = new Option(newUtzVal, newUtzVal, true, true);
						// Append it to the select
						$("#ustadz").append(newState).trigger('change');
					} 
				});  
			});
		</script>
		<script>
			$(document).ready(function(){
				$("#replay").click(function(){
					$("#replay-panel").toggle();
				});
			});
		</script>
	</body>
</html>