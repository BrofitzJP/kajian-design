<div class="nav-content nav-bottom">
	<ul class="tabs bottom-bar" id="tabs-swipe-demo">
		<li class="tab" style="display: none;"><a href="#test-swipe-1">Beranda</a></li>
		<li class="tab"><a href="#test-swipe-2"><i class="material-icons">add_circle</i> Kajian</a></li>
		<li class="tab"><a href="#test-swipe-3"><i class="material-icons">add_circle</i> Ustadz</a></li>
		<li class="tab"><a href="#test-swipe-4"><i class="material-icons">add_circle</i> Masjid</a></li>
	</ul>
</div>

<?php $this->load->view('frontpage/add_kajian'); ?>
<?php $this->load->view('frontpage/add_ustadz'); ?>
<?php $this->load->view('frontpage/add_masjid'); ?>