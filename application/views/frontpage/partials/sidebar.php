<ul id="slide-out" class="side-nav cst-nav">
	<li>
		<div class="user-view">
			<div class="background">
				<img src="<?php echo site_url('') ?>/assets/images/bg-profile.jpg">
			</div>
			<a href="#!user"><img class="circle" src="<?php echo site_url('') ?>/assets/images/profile.jpg"></a>
			<a href="#!name"><span class="white-text name">John Doe</span></a>
			<a href="#!email"><span class="white-text email">brofitzjp@gmail.com</span></a>
		</div>
	</li>
	<li><a class="waves-effect" href="<?php echo site_url() ?>frontpage"><i class="material-icons">assignment</i>Kajian</a></li>
	<li><a class="waves-effect" href="<?php echo site_url() ?>frontpage/ustadz"><i class="material-icons">supervisor_account</i>Ustadz</a></li>
	<li><a class="waves-effect" href="<?php echo site_url() ?>frontpage/masjid"><i class="material-icons">brightness_4</i>Masjid</a></li>
	<li><a class="waves-effect" href="#!"><i class="material-icons">donut_small</i>Donasi</a></li>
	<li><a class="waves-effect" href="#!"><i class="material-icons">build</i>Support</a></li>
	<li><a href="#!"><i class="material-icons">account_circle</i>Profile</a></li>
	<li><a href="#!"><i class="material-icons">exit_to_app</i>Logout</a></li>

	<!-- Pages -->
	<li><div class="divider"></div></li>
	<li><a class="waves-effect" href="<?php echo site_url() ?>frontpage">Kajian</a></li>
	<li><a class="waves-effect" href="<?php echo site_url() ?>frontpage/edit_kajian">Edit Kajian</a></li>
	<li><a class="waves-effect" href="<?php echo site_url() ?>frontpage/result_kajian">Hasil Pencarian Kajian</a></li>
	<li><a class="waves-effect" href="<?php echo site_url() ?>frontpage/ustadz">Ustadz</a></li>
	<li><a class="waves-effect" href="<?php echo site_url() ?>frontpage/detail_ustadz">Detail Ustadz</a></li>
	<li><a class="waves-effect" href="<?php echo site_url() ?>frontpage/edit_ustadz">Edit Ustadz</a></li>
	<li><a class="waves-effect" href="<?php echo site_url() ?>frontpage/result_ustadz">Hasil Pencarian Ustadz</a></li>
	<li><a class="waves-effect" href="<?php echo site_url() ?>frontpage/masjid">Masjid</a></li>
	<li><a class="waves-effect" href="<?php echo site_url() ?>frontpage/detail_masjid">Detail Masjid</a></li>
	<li><a class="waves-effect" href="<?php echo site_url() ?>frontpage/edit_masjid">Edit Masjid</a></li>
	<li><a class="waves-effect" href="<?php echo site_url() ?>frontpage/donasi">Donasi</a></li>
	<li><a class="waves-effect" href="<?php echo site_url() ?>frontpage/user">Profile</a></li>
	<li><a class="waves-effect" href="<?php echo site_url() ?>frontpage/edit_user">Edit Profile</a></li>
	<li><a class="waves-effect" href="<?php echo site_url() ?>frontpage/support">Support</a></li>
	<li><a class="waves-effect" href="<?php echo site_url() ?>frontpage/add_support">Add Support</a></li>
	<li><a class="waves-effect" href="<?php echo site_url() ?>frontpage/detail_support">Detail Support</a></li>
	<li><a class="waves-effect" href="<?php echo site_url() ?>frontpage/login">Login</a></li>
	<li><a class="waves-effect" href="<?php echo site_url() ?>frontpage/register">Register</a></li>
	<li><a class="waves-effect" href="<?php echo site_url() ?>frontpage/forgot_password">Forgot Password</a></li>
</ul>