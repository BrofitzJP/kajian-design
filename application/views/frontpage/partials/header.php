<!-- Navbar -->
<nav class="nav-extended teal cst-navbar-app">
	<div class="nav-wrapper">
		<a href="#" data-activates="slide-out" class="button-collapse"><i class="material-icons">menu</i></a>
		<ul class="right">
			<li><a id="pageRefresh"><i class="material-icons">refresh</i></a></li>
			<li>
				<a href="#" data-activates="dropdown1" class="dropdown-button"><i class="material-icons">more_vert</i></a>
				<ul id="dropdown1" class="dropdown-content cst-dropdown">
					<li><a href="#!">Tambah Ustad</a></li>
					<li><a href="#!">Tambah Kajian</a></li>
					<li><a href="#!">Tambah Masjid</a></li>
				</ul>
			</li>
		</ul>
	</div>
</nav>