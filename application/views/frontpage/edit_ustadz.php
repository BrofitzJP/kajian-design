<div class="top-page">
	<div class="title-page">Edit Ustadz</div>
</div>

<!-- Content Page -->
<div class="container app-content">
	<div class="row">
		<div class="col s12">
			<!-- Form -->
			<form action="" method="">
				<ul class="collection app-collection">
					<li class="collection-item avatar e-u-avatar">
						<div class="avatar-ustadz">
							<img src="<?php echo site_url('') ?>/assets/images/abdul_somad_300x300.jpg" alt="">						
						</div>
						<span class="title">Abdul Somad Lc. MA</span>
						<!-- Radio button -->
						<div class="verification">
							<input class="with-gap" name="group1" type="radio" id="test1" checked="" />
							<label for="test1">Verified</label>
							<input class="with-gap" name="group1" type="radio" id="test2"  />
							<label class="un-v" for="test2">Unverified</label>
						</div>

					</li>
				</ul>

				<!-- Form Edit Ustadz -->
				<div class="card-panel app-card">
					<div class="row">
						<div class="input-field col s12">
							<i class="material-icons prefix">person</i>
							<input id="name-ustadz" name="" type="text" value="Abdul Somad Lc. MA">
							<label for="name-ustadz">Nama Ustadz</label>
						</div>
						<div class="input-field col s12">
							<i class="material-icons prefix">school</i>
							<input id="last-edu" name="" type="text" value="Maroko">
							<label for="last-edu">Pendidikan Terakhir</label>
						</div>
						<div class="input-field col s12">
							<i class="material-icons prefix">public</i>
							<input id="last-edu" name="" type="text" value="Pegawai Negeri Sipil">
							<label for="last-edu">Jabatan</label>
						</div>
						<div class="file-field input-field col s12">
							<div class="btn btn-small red">
								<i class="material-icons">attachment</i>
								<input type="file">
							</div>
							<div class="file-path-wrapper">
								<input class="file-path" type="text" placeholder="Ambil gambar ustadz baru" value="foto-ustadz.jpg">
							</div>
						</div>
						<div class="col s12">
							<button class="btn waves-effect waves-light right" type="submit" name="action">Submit
								<i class="material-icons right">send</i>
							</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>