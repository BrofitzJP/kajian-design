<div class="top-page">
	<div class="title-page">8 Masjid</div>
	<a class="modal-trigger" href="<?php echo site_url() ?>frontpage/user"><i class="material-icons">arrow_back</i></a>
</div>

<!-- Content Page -->
<div class="container app-content">
	<div class="row">
		<div class="col s12">
			<div class="card-panel app-card teal">
				<table class="search-kajian">
					<tr>
						<td>8 masjid ditemukan pada akun Anda</td>
					</tr>
				</table>
			</div>
			<!-- List Masjid -->
			<ul class="collection app-coll-mosque">
				<li class="collection-item">
					<img src="<?php echo site_url('') ?>assets/images/mosque_300x300.jpg" alt="">
					<span class="title">Masjid Al-Ikhlas</span>
					<p><span>Kec. Mojogedang, Kab. Mojogedang, Prov. Jawa Tengah</span></p>
					<a href="#" class="secondary-content-app waves-effect waves-light btn btn-small">Detail</a>
				</li>
				<li class="collection-item">
					<img src="<?php echo site_url('') ?>assets/images/mosque_300x300.jpg" alt="">
					<span class="title">Masjid Al-Ikhlas</span>
					<p><span>Kec. Mojogedang, Kab. Mojogedang, Prov. Jawa Tengah</span></p>
					<a href="#" class="secondary-content-app waves-effect waves-light btn btn-small">Detail</a>
				</li>
				<li class="collection-item">
					<img src="<?php echo site_url('') ?>assets/images/mosque_300x300.jpg" alt="">
					<span class="title">Masjid Al-Ikhlas</span>
					<p><span>Kec. Mojogedang, Kab. Mojogedang, Prov. Jawa Tengah</span></p>
					<a href="#" class="secondary-content-app waves-effect waves-light btn btn-small">Detail</a>
				</li>
				<li class="collection-item">
					<img src="<?php echo site_url('') ?>assets/images/mosque_300x300.jpg" alt="">
					<span class="title">Masjid Al-Ikhlas</span>
					<p><span>Kec. Mojogedang, Kab. Mojogedang, Prov. Jawa Tengah</span></p>
					<a href="#" class="secondary-content-app waves-effect waves-light btn btn-small">Detail</a>
				</li>
				<li class="collection-item">
					<img src="<?php echo site_url('') ?>assets/images/mosque_300x300.jpg" alt="">
					<span class="title">Masjid Al-Ikhlas</span>
					<p><span>Kec. Mojogedang, Kab. Mojogedang, Prov. Jawa Tengah</span></p>
					<a href="#" class="secondary-content-app waves-effect waves-light btn btn-small">Detail</a>
				</li>
				<li class="collection-item">
					<img src="<?php echo site_url('') ?>assets/images/mosque_300x300.jpg" alt="">
					<span class="title">Masjid Al-Ikhlas</span>
					<p><span>Kec. Mojogedang, Kab. Mojogedang, Prov. Jawa Tengah</span></p>
					<a href="#" class="secondary-content-app waves-effect waves-light btn btn-small">Detail</a>
				</li>
				<li class="collection-item">
					<img src="<?php echo site_url('') ?>assets/images/mosque_300x300.jpg" alt="">
					<span class="title">Masjid Al-Ikhlas</span>
					<p><span>Kec. Mojogedang, Kab. Mojogedang, Prov. Jawa Tengah</span></p>
					<a href="#" class="secondary-content-app waves-effect waves-light btn btn-small">Detail</a>
				</li>
				<li class="collection-item">
					<img src="<?php echo site_url('') ?>assets/images/mosque_300x300.jpg" alt="">
					<span class="title">Masjid Al-Ikhlas</span>
					<p><span>Kec. Mojogedang, Kab. Mojogedang, Prov. Jawa Tengah</span></p>
					<a href="#" class="secondary-content-app waves-effect waves-light btn btn-small">Detail</a>
				</li>
			</ul>
		</div>

		<!-- Modal Search Ustadz -->
		<div id="search_masjid" class="modal modal-app">
			<form class="col s12">
				<div class="modal-content">
					<h5>Cari Masjid</h5>
					<div class="row">
						<div class="input-field col s12">
							<input id="name-mosque" name="" type="text">
							<label for="name-mosque">Nama Masjid</label>
						</div>
						<div class="input-field select2-no-prefix col s12">
							<select class="select2">
								<option value="" disabled selected>Pilih Provinsi</option>
								<option value="1">Banten</option>
								<option value="2">Jakarta</option>
								<option value="3">Jawa Barat</option>
								<option value="4">Jawa Tengah</option>
								<option value="5">Yogyakarta</option>
								<option value="6">Jawa Timur</option>
							</select>
							<label class="label-select2">Pilih Provinsi</label>
						</div>
						<div class="input-field select2-no-prefix col s12">
							<select class="select2">
								<option value="" disabled selected>Pilih Kabupaten</option>
								<option value="1">Banten</option>
								<option value="2">Jakarta</option>
								<option value="3">Jawa Barat</option>
								<option value="4">Jawa Tengah</option>
								<option value="5">Yogyakarta</option>
								<option value="6">Jawa Timur</option>
							</select>
							<label class="label-select2">Pilih Kabupaten</label>
						</div>
						<div class="input-field select2-no-prefix col s12">
							<select class="select2">
								<option value="" disabled selected>Pilih Kacamatan</option>
								<option value="1">Banten</option>
								<option value="2">Jakarta</option>
								<option value="3">Jawa Barat</option>
								<option value="4">Jawa Tengah</option>
								<option value="5">Yogyakarta</option>
								<option value="6">Jawa Timur</option>
							</select>
							<label class="label-select2">Pilih Kecamatan</label>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button class="btn waves-effect waves-light right" type="submit" name="action">Cari
						<i class="material-icons right">search</i>
					</button>
				</div>
			</form>
		</div>
	</div>
</div>