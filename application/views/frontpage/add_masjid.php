<div id="test-swipe-4">
	<div class="title-page">Tambah Masjid</div>

	<!-- Content Page -->
	<div class="container app-content">
		<div class="row">
			<div class="col s12">
				<div class="card-panel app-card">
					<div class="row">
						<!-- Add Kajian -->
						<form class="col s12">
							<div class="row">
								<div class="input-field col s12">
									<i class="material-icons prefix">brightness_4</i>
									<input id="name-mosque" name="" type="text">
									<label for="name-mosque">Nama Masjid</label>
								</div>
								<div class="input-field col s12">
									<i class="material-icons prefix">location_on</i>
									<select class="select2">
										<option value="" disabled selected>Pilih Provinsi</option>
										<option value="1">Banten</option>
										<option value="2">Jakarta</option>
										<option value="3">Jawa Barat</option>
										<option value="4">Jawa Tengah</option>
										<option value="5">Yogyakarta</option>
										<option value="6">Jawa Timur</option>
									</select>
									<label class="label-select2">Pilih Provinsi</label>
								</div>
								<div class="input-field col s12">
									<i class="material-icons prefix">location_on</i>
									<select class="select2">
										<option value="" disabled selected>Pilih Kabupaten</option>
										<option value="1">Banten</option>
										<option value="2">Jakarta</option>
										<option value="3">Jawa Barat</option>
										<option value="4">Jawa Tengah</option>
										<option value="5">Yogyakarta</option>
										<option value="6">Jawa Timur</option>
									</select>
									<label class="label-select2">Pilih Kabupaten</label>
								</div>
								<div class="input-field col s12">
									<i class="material-icons prefix">location_on</i>
									<select class="select2">
										<option value="" disabled selected>Pilih Kacamatan</option>
										<option value="1">Banten</option>
										<option value="2">Jakarta</option>
										<option value="3">Jawa Barat</option>
										<option value="4">Jawa Tengah</option>
										<option value="5">Yogyakarta</option>
										<option value="6">Jawa Timur</option>
									</select>
									<label class="label-select2">Pilih Kecamatan</label>
								</div>
								<div class="input-field col s12">
									<i class="material-icons prefix">my_location</i>
									<input id="address" name="" type="text">
									<label for="address">Alamat</label>
								</div>
								<div class="input-field col s12">
									<i class="material-icons prefix">person</i>
									<input id="name-takmir" name="" type="text">
									<label for="name-takmir">Nama Takmir</label>
								</div>
								<div class="input-field col s12">
									<i class="material-icons prefix">phone</i>
									<input id="contact1" name="" type="text">
									<label for="contact1">Kontak Takmir</label>
								</div>
								<div class="input-field col s12">
									<i class="material-icons prefix">phone</i>
									<input id="contact2" name="" type="tel">
									<label for="contact2">Kontak Tekmir II</label>
								</div>
								<div class="input-field col s12">
									<i class="material-icons prefix">email</i>
									<input id="email" name="" type="email">
									<label for="email">Email</label>
								</div>
								<div class="input-field col s12">
									<i class="material-icons prefix">people</i>
									<input id="email" name="" type="email">
									<label for="email">Jumlah Jamaah Subuh</label>
								</div>
								<div class="input-field col s12">
									<i class="material-icons prefix">link</i>
									<input id="gmap" name="" type="url">
									<label for="gmap">Link Google Map</label>
								</div>
								<div class="file-field input-field col s12">
									<div class="btn btn-small red">
										<i class="material-icons">attachment</i>
										<input type="file">
									</div>
									<div class="file-path-wrapper">
										<input class="file-path" type="text" placeholder="Upload foto masjid">
									</div>
								</div>
							</div>
							<button class="btn waves-effect waves-light right" type="submit" name="action">Submit
								<i class="material-icons right">send</i>
							</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>