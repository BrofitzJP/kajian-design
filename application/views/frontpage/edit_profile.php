<div class="top-page">
	<div class="title-page">Edit Profile</div>
	<a class="modal-trigger" href="<?php echo site_url() ?>frontpage/user"><i class="material-icons">arrow_back</i></a>
</div>

<!-- Content Page -->
<div class="container app-content">
	<div class="container m-t-10">
		<div class="row">
			<div class="card-panel app-card">
				<div class="row">
					<!-- Edit Profile -->
					<form class="col s12">
						<div class="row">
							<div class="input-field col s12">
								<i class="material-icons prefix">person</i>
								<input id="name-ustadz" name="" type="text" value="Brofit J">
								<label for="name-ustadz">Nama</label>
							</div>
							<div class="input-field col s12">
								<i class="material-icons prefix">account_circle</i>
								<input id="name-ustadz" name="" type="text" value="brofit">
								<label for="name-ustadz">Username</label>
							</div>
							<div class="input-field col s12">
								<i class="material-icons prefix">email</i>
								<input id="name-ustadz" name="" type="text" value="brofitzjp@gmail.com">
								<label for="name-ustadz">Email</label>
							</div>
							<div class="input-field col s12">
								<i class="material-icons prefix">date_range</i>
								<input id="start-date" type="text" class="datepicker" value="24 January, 2018">
								<label for="start-date">Tanggal Lahir</label>
							</div>
							<div class="input-field col s12">
								<i class="material-icons prefix">my_location</i>
								<input id="address" name="" type="text" value="Mojogedang RT03/RW02">
								<label for="address">Alamat</label>
							</div>
							<div class="col s12 rb-form">
								<i class="material-icons prefix">person</i>
								<input class="with-gap" name="group1" type="radio" id="test1" checked="" />
								<label for="test1">Laki - Laki</label>
								<input class="with-gap" name="group1" type="radio" id="test2"  />
								<label class="un-v" for="test2">Perempuan</label>
							</div>
							<div class="input-field col s12">
								<i class="material-icons prefix">location_on</i>
								<select class="select2">
									<option value="" disabled>Pilih Provinsi</option>
									<option value="1">Banten</option>
									<option value="2">Jakarta</option>
									<option value="3">Jawa Barat</option>
									<option value="4" selected>Jawa Tengah</option>
									<option value="5">Yogyakarta</option>
									<option value="6">Jawa Timur</option>
								</select>
								<label class="label-select2">Pilih Provinsi</label>
							</div>
							<div class="input-field col s12">
								<i class="material-icons prefix">location_on</i>
								<select class="select2">
									<option value="" disabled selected>Pilih Kabupaten</option>
									<option value="1" selected>Karanganyar</option>
									<option value="2">Jakarta</option>
									<option value="3">Jawa Barat</option>
									<option value="4">Jawa Tengah</option>
									<option value="5">Yogyakarta</option>
									<option value="6">Jawa Timur</option>
								</select>
								<label class="label-select2">Pilih Kabupaten</label>
							</div>
							<div class="input-field col s12">
								<i class="material-icons prefix">location_on</i>
								<select class="select2">
									<option value="" disabled selected>Pilih Kacamatan</option>
									<option value="1" selected>Mojogedang</option>
									<option value="2">Jakarta</option>
									<option value="3">Jawa Barat</option>
									<option value="4">Jawa Tengah</option>
									<option value="5">Yogyakarta</option>
									<option value="6">Jawa Timur</option>
								</select>
								<label class="label-select2">Pilih Kecamatan</label>
							</div>
							<dic class="col s12">
								<span class="e-p-photo">Unggah foto profile Anda</span>
							</dic>
							<div class="file-field input-field col s12">
								<div class="btn btn-small red">
									<i class="material-icons">attachment</i>
									<input type="file">
								</div>
								<div class="file-path-wrapper">
									<input class="file-path" type="text" placeholder="Upload foto masjid" value="foto-profile.jpg">
								</div>
							</div>
							<dic class="col s12">
								<span class="e-p-photo">Unggah foto sampul Anda</span>
							</dic>
							<div class="file-field input-field col s12">
								<div class="btn btn-small red">
									<i class="material-icons">attachment</i>
									<input type="file">
								</div>
								<div class="file-path-wrapper">
									<input class="file-path" type="text" placeholder="Upload foto sampul" value="foto-sampul.jpg">
								</div>
							</div>
						</div>
						<button class="btn waves-effect waves-light right" type="submit" name="action">Update
							<i class="material-icons right">send</i>
						</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>