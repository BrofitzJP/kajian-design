<div class="title-page">Detail Inbox</div>

<!-- Content Page -->
<div class="container app-content">
	<div class="row">
		<div class="col s12">
			<div class="card-panel app-support">
				<div class="card-header">
					<ul class="collection app-support">
						<li class="collection-item avatar">
							<img src="<?php echo site_url('') ?>/assets/images/profile.jpg" alt="" class="circle">
							<span class="title st">Bagaimana Cara Add Ustadz?</span>
							<p>
								<span class="st">Alexandria</span><br>
								Support
							</p>
							<a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
							<span class="secondary-content">2m</span>
						</li>
					</ul>
				</div>
				<div class="card-body-support">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam consectetur neque quisquam aspernatur, quibusdam soluta voluptates aperiam, nostrum ratione laudantium facere in iusto eius officiis excepturi, ea odit dignissimos unde.</p>
					<span>Attachments : </span> <a href="#"><span>Lampiran.png</span> <i class="material-icons atch">file_download</i></a>
				</div>
			</div>

			<!-- Editor -->
			<div class="text-center m-b-20">
				<a type="button" id="replay" class="waves-effect waves-light btn btn-front">Replay</a>
			</div>
			<div class="card-panel app-card" id="replay-panel" style="display: none;">
				<div class="row">
					<!-- Add Kajian -->
					<form class="col s12">
						<div class="row">
							<div class="input-field col s12">
								<input id="title" name="" type="text" value="">
								<label for="title">Judul</label>
							</div>
							<div class="input-field col s12">
								<textarea id="messegge" class="materialize-textarea" data-length="520"></textarea>
								<label for="messegge">Tulis Pesan</label>
							</div>
							<div class="file-field input-field col s12">
								<div class="btn btn-small red">
									<i class="material-icons">attachment</i>
									<input type="file">
								</div>
								<div class="file-path-wrapper">
									<input class="file-path" type="text" placeholder="Unggah Lampiran">
								</div>
							</div>
						</div>
						<button class="btn waves-effect waves-light right" type="submit" name="action">Kirim
							<i class="material-icons right">send</i>
						</button>
					</form>
				</div>
			</div>

			<!-- Comment -->
			<ul class="collection chat-support">
				<li class="collection-item avatar">
					<img src="<?php echo site_url('') ?>/assets/images/abdul_somad_300x300.jpg" alt="" class="circle">
					<span class="title">Brofit J</span>
					<p><span class="chat-inner">Administrator | 12/12/2017 19:50</span><br>
				 		Quam consectetur neque quisquam aspernatur
					</p>
				</li>
				<li class="collection-item avatar">
					<img src="<?php echo site_url('') ?>/assets/images/profile.jpg" alt="" class="circle">
					<span class="title">Alexandria</span>
					<p><span class="chat-inner">Participant | 12/12/2017 19:50</span><br>
				 		Lorem ipsum dolor sit amet, consectetur adipisicing elit
					</p>
				</li>
				<li class="collection-item avatar">
					<img src="<?php echo site_url('') ?>/assets/images/abdul_somad_300x300.jpg" alt="" class="circle">
					<span class="title">Brofit J</span>
					<p><span class="chat-inner">Administrator | 12/12/2017 19:50</span><br>
				 		Nostrum ratione laudantium 
					</p>
					<span>Attachments : </span> <a href="#"><span>Lampiran.png</span> <i class="material-icons atch">file_download</i></a>
				</li>
			</ul>

		</div>
	</div>
</div>