<div class="top-page">
	<div class="title-page">Support</div>
</div>

<!-- Content Page -->
<div class="container app-content">
	<div class="row">
		<div class="col s12">
			<strong>Today</strong>
			<ul class="collection app-support">
				<a href="#" class="">
					<li class="collection-item avatar waves-effect read">
						<img src="<?php echo site_url('') ?>/assets/images/profile.jpg" alt="" class="circle">
						<span class="title st">Bagaimana Cara Add Ustadz?</span>
						<p>
							<span class="st">Brofit J</span>
							- <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit..</span>
						</p>
						<a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
						<span class="secondary-content">2m</span>
					</li>
				</a>
				<a href="#" class="">
					<li class="collection-item avatar waves-effect read">
						<img src="<?php echo site_url('') ?>/assets/images/profile.jpg" alt="" class="circle">
						<span class="title st">Bagaimana Cara Add Ustadz?</span>
						<p>
							<span class="st">Brofit J</span>
							- <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit..</span>
						</p>
						<a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
						<span class="secondary-content">15m</span>
					</li>
				</a>
				<a href="#" class="">
					<li class="collection-item avatar waves-effect read">
						<img src="<?php echo site_url('') ?>/assets/images/profile.jpg" alt="" class="circle">
						<span class="title st">Bagaimana Cara Add Ustadz?</span>
						<p>
							<span class="st">Brofit J</span>
							- <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit..</span>
						</p>
						<a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
						<span class="secondary-content">30m</span>
					</li>
				</a>
				<a href="#" class="">
					<li class="collection-item avatar waves-effect">
						<img src="<?php echo site_url('') ?>/assets/images/profile.jpg" alt="" class="circle">
						<span class="title st">Bagaimana Cara Add Ustadz?</span>
						<p>
							<span class="st">Brofit J</span>
							- <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit..</span>
						</p>
						<a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
						<span class="secondary-content">4h</span>
					</li>
				</a>
				<a href="#" class="">
					<li class="collection-item avatar waves-effect">
						<img src="<?php echo site_url('') ?>/assets/images/profile.jpg" alt="" class="circle">
						<span class="title st">Bagaimana Cara Add Ustadz?</span>
						<p>
							<span class="st">Brofit J</span>
							- <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit..</span>
						</p>
						<a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
						<span class="secondary-content">5h</span>
					</li>
				</a>
			</ul>
			<strong>Yesterday</strong>
			<ul class="collection app-support">
				<a href="#" class="">
					<li class="collection-item avatar waves-effect">
						<img src="<?php echo site_url('') ?>/assets/images/profile.jpg" alt="" class="circle">
						<span class="title st">Bagaimana Cara Add Ustadz?</span>
						<p>
							<span class="st">Brofit J</span>
							- <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit..</span>
						</p>
						<a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
						<span class="secondary-content">2m</span>
					</li>
				</a>
				<a href="#" class="">
					<li class="collection-item avatar waves-effect">
						<img src="<?php echo site_url('') ?>/assets/images/profile.jpg" alt="" class="circle">
						<span class="title st">Bagaimana Cara Add Ustadz?</span>
						<p>
							<span class="st">Brofit J</span>
							- <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit..</span>
						</p>
						<a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
						<span class="secondary-content">2d</span>
					</li>
				</a>
				<a href="#" class="">
					<li class="collection-item avatar waves-effect">
						<img src="<?php echo site_url('') ?>/assets/images/profile.jpg" alt="" class="circle">
						<span class="title st">Bagaimana Cara Add Ustadz?</span>
						<p>
							<span class="st">Brofit J</span>
							- <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit..</span>
						</p>
						<a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
						<span class="secondary-content">3d</span>
					</li>
				</a>
				<a href="#" class="">
					<li class="collection-item avatar waves-effect">
						<img src="<?php echo site_url('') ?>/assets/images/profile.jpg" alt="" class="circle">
						<span class="title st">Bagaimana Cara Add Ustadz?</span>
						<p>
							<span class="st">Brofit J</span>
							- <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit..</span>
						</p>
						<a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
						<span class="secondary-content">3d</span>
					</li>
				</a>
				<a href="#" class="">
					<li class="collection-item avatar">
						<img src="<?php echo site_url('') ?>/assets/images/profile.jpg" alt="" class="circle">
						<span class="title st">Bagaimana Cara Add Ustadz?</span>
						<p>
							<span class="st">Brofit J</span>
							- <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit..</span>
						</p>
						<a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
						<span class="secondary-content">3d</span>
					</li>
				</a>
			</ul>
		</div>
	</div>
</div>