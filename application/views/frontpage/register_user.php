<!doctype html>
<html lang="en">
	<head>
		<title>Register</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- CSS -->
		<link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
		<link rel="stylesheet" href="<?php echo site_url('') ?>assets/frameworks/materialize/css/materialize.min.css" crossorigin="anonymous">
		<link rel="stylesheet" href="<?php echo site_url('') ?>assets/css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body class="register">

		<!-- Register Page -->
		<div class="container">
			<div class="row">
				<div class="col s12">
					<div class="card-panel login-card register">
						<div class="login-top-content">
							<h5><i class="material-icons">person_add</i> Register</h5>
						</div>
						<form>
							<div class="row">
								<div class="input-field col s12">
									<input id="name" type="text" class="validate">
									<label for="name">Nama Lengkap</label>
								</div>
								<div class="input-field col s12">
									<input id="email" type="email" class="validate">
									<label for="email">Email</label>
								</div>
								<div class="input-field col s12">
									<input id="username" type="text" class="validate">
									<label for="username">Username</label>
								</div>
								<div class="input-field col s12">
									<input id="password" type="text" class="validate">
									<label for="password">Password</label>
								</div>
								<div class="input-field col s12">
									<input id="retype-password" type="text" class="validate">
									<label for="retype-password">Ulangi Password</label>
								</div>
								<div class="col s12">
									<p>
										<input type="checkbox" class="filled-in" id="filled-in-box"/>
										<label for="filled-in-box"><span>I read and agree to the <a href="#">term of usage.</a></span></label>
									</p>
								</div>
								<div class="col s12">
									<a class="waves-effect waves-light btn btn-front btn-block m-t-20">Sign Up</a>
								</div>
							</div>
							<div class="row reg-bottom">
								<a href="<?php echo site_url() ?>frontpage/login">You already have a membership?</a>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	
		<!-- JavaScript -->
		<script src="<?php echo site_url() ?>assets/js/jquery.min.js" crossorigin="anonymous"></script>
		<script src="<?php echo site_url() ?>assets/frameworks/materialize/js/materialize.min.js" crossorigin="anonymous"></script>
	</body>
</html>