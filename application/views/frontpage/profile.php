<div class="top-page">
	<div class="title-page">Profile</div>
</div>

<!-- Content Page -->
<div class="all-profile-content">
	<div class="cover-image">
		<div class="inner-cover">
			<h4 class="title-profile">Brofit J</h4>
			<p>brofitzp@gmail.com</p>
		</div>
		<img src="<?php echo site_url() ?>/assets/images/bg-profile.jpg" alt="">
	</div>
	<div class="foto-profile">
		<img src="<?php echo site_url() ?>/assets/images/profile.jpg" alt="">
	</div>
	<div class="app-content pofile-content">
		<div class="row">
			<div class="col s4">
				<a href="<?php echo site_url() ?>frontpage/user_kajian">
					<h4>24</h4>
					<span>Post Kajian</span>
				</a>
			</div>
			<div class="col s4">
				<a href="<?php echo site_url() ?>frontpage/user_ustadz">
					<h4>27</h4>
					<span>Post Ustadz</span>
				</a>
			</div>
			<div class="col s4">
				<a href="<?php echo site_url() ?>frontpage/user_masjid">
					<h4>8</h4>
					<span>Post Majid</span>
				</a>
			</div>
		</div>
	</div>
	<div class="container m-t-10">
		<div class="row">
			<div class="col s12">
				<ul class="collection col-profile">
					<li class="collection-item avatar">
						<div class="row">
							<div class="col s2">
								<i class="material-icons">person</i>
							</div>
							<div class="col s10">
								<span>Nama Langkap</span>
								<p>Brofit Joko Purnomo</p>
							</div>
						</div>
					</li>
					<li class="collection-item avatar">
						<div class="row">
							<div class="col s2">
								<i class="material-icons">account_circle</i>
							</div>
							<div class="col s10">
								<span>Username</span>
								<p>brofit</p>
							</div>
						</div>
					</li>
					<li class="collection-item avatar">
						<div class="row">
							<div class="col s2">
								<i class="material-icons">email</i>
							</div>
							<div class="col s10">
								<span>Email</span>
								<p>brofitzjp@gmail.com</p>
							</div>
						</div>
					</li>
					<li class="collection-item avatar">
						<div class="row">
							<div class="col s2">
								<i class="material-icons">date_range</i>
							</div>
							<div class="col s10">
								<span>Tanggal Lahir</span>
								<p>22 Oktober 1997</p>
							</div>
						</div>
					</li>
					<li class="collection-item avatar">
						<div class="row">
							<div class="col s2">
								<i class="material-icons">accessibility</i>
							</div>
							<div class="col s10">
								<span>Jenis Kelamin</span>
								<p>Laki - Laki</p>
							</div>
						</div>
					</li>
					<li class="collection-item avatar">
						<div class="row">
							<div class="col s2">
								<i class="material-icons">place</i>
							</div>
							<div class="col s10">
								<span>Alamat</span>
								<p>Mojogedang RT03/RW02, Kec. Mojogedang, Kab. Karanganyar, Prov. Jawa Tengah</p>
							</div>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<a href="<?php echo site_url() ?>frontpage/edit_user" class="btn-floating btn-large red btn-profile-edit"><i class="large material-icons">mode_edit</i></a>
</div>