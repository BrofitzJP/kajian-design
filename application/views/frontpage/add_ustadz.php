<div id="test-swipe-3">
	<div class="title-page">Add Ustadz</div>

	<!-- Content Page -->
	<div class="container app-content">
		<div class="row">
			<div class="col s12">
				<div class="card-panel app-card">
					<div class="row">
						<!-- Add Kajian -->
						<form class="col s12">
							<div class="row">
								<div class="input-field col s12">
									<i class="material-icons prefix">person</i>
									<input id="name-ustadz" name="" type="text" value="">
									<label for="name-ustadz">Nama Ustadz</label>
								</div>
								<div class="input-field col s12">
									<i class="material-icons prefix">school</i>
									<input id="last-edu" name="" type="text" value="">
									<label for="last-edu">Pendidikan Terakhir</label>
								</div>
								<div class="input-field col s12">
									<i class="material-icons prefix">public</i>
									<input id="jabatan" name="" type="text" value="">
									<label for="jabatan">Jabatan</label>
								</div>
								<div class="file-field input-field col s12">
									<div class="btn btn-small red">
										<i class="material-icons">attachment</i>
										<input type="file">
									</div>
									<div class="file-path-wrapper">
										<input class="file-path" type="text" placeholder="Upload foto ustadz">
									</div>
								</div>
							</div>
							<button class="btn waves-effect waves-light right" type="submit" name="action">Submit
								<i class="material-icons right">send</i>
							</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>