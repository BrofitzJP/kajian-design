<div class="title-page">Edit Kajian</div>

<!-- Content Page -->
<div class="container app-content">
	<div class="row">
		<div class="col s12">
			<div class="card-panel app-card">
				<div class="row">
					<!-- Add Kajian -->
					<form class="col s12">
						<div class="row">
							<div class="input-field col s12">
								<i class="material-icons prefix">title</i>
								<input id="title-kajian" name="" type="text" value="Menuju Keluarga Bahagia Dunia Akhirat">
								<label for="title-kajian">Tema Kajian</label>
							</div>
							<div class="col s12">
								<span><em>Jika pilihan ustadz belum tersedia silahkan tambahkan ustadz</em></span>
							</div>
							<div class="input-field add-select-utz col s12">
								<i class="material-icons prefix">account_circle</i>
								<select id="ustadz">
									<option value="" disabled selected>Pilih Ustadz</option>
									<option value="1" selected>Abdul Somad Lc. MA</option>
									<option value="2">Al-Ikhlas</option>
									<option value="3">Al-Aqsa</option>
								</select>
								<input id="new-utz" type="text" placeholder="Tambahkan ustadz" />
								<button class="btn waves-effect waves-light btn-small" id="btn-add-utz" type="button" name="action">Add</button>
							</div>
							<div class="input-field col s12">
								<i class="material-icons prefix" style="margin-top: 12px;">brightness_4</i>
								<select class="select2">
									<option value="" disabled selected>Pilih Masjid</option>
									<option value="1" selected>At-Taqwa</option>
									<option value="2">Al-Ikhlas</option>
									<option value="3">Al-Aqsa</option>
								</select>
								<label class="label-select2">Masjid</label>
							</div>
							<div class="input-field col s12">
								<i class="material-icons prefix">date_range</i>
								<input id="date-kajian" type="text" class="datepicker" value="24 Januari 2018">
								<label for="date-kajian">Tanggal Kajian</label>
							</div>
							<div class="input-field col s12">
								<i class="material-icons prefix">alarm</i>
								<input id="time-begin" type="text" class="timepicker" value="08:00">
								<label for="time-begin">Jam Mulai</label>
							</div>
							<div class="input-field col s12">
								<i class="material-icons prefix">alarm_on</i>
								<input id="time-finish" type="text" class="timepicker" value="10.00">
								<label for="time-finish">Jam Selesai</label>
							</div>
							<div class="file-field input-field col s12">
								<div class="btn btn-small red">
									<i class="material-icons">attachment</i>
									<input type="file">
								</div>
								<div class="file-path-wrapper">
									<input class="file-path" type="text" placeholder="Ambil gambar dari local" value="kajian.png">
								</div>
							</div>
						</div>

						<button class="btn waves-effect waves-light right" type="submit" name="action">Update
							<i class="material-icons right">cached</i>
						</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>