<div class="top-page">
	<div class="title-page">Ustadz</div>
	<a class="modal-trigger" href="#search_ustadz"><i class="material-icons">search</i></a>
</div>

<!-- Content Page -->
<div class="container app-content">
	<div class="row">
		<div class="col s12">
			<!-- List Ustadz -->
			<ul class="collection app-collection">
				<li class="collection-item avatar">
					<div class="avatar-ustadz">
						<img src="<?php echo site_url('') ?>/assets/images/abdul_somad_300x300.jpg" alt="">						
					</div>
					<span class="title">Abdul Somad Lc. MA</span>
					<p>2 Kajian <br>
						<span class="utz-verified">Verified</span>
					</p>
					<a href="#" class="secondary-content-app waves-effect waves-light btn btn-small">Detail</a>
				</li>
				<li class="collection-item avatar">
					<div class="avatar-ustadz">
						<img src="<?php echo site_url('') ?>/assets/images/abdul_somad_300x300.jpg" alt="">						
					</div>
					<span class="title">Abdul Somad Lc. MA</span>
					<p>2 Kajian <br>
						<span class="utz-verified">Verified</span>
					</p>
					<a href="#" class="secondary-content-app waves-effect waves-light btn btn-small">Detail</a>
				</li>
				<li class="collection-item avatar">
					<div class="avatar-ustadz">
						<img src="<?php echo site_url('') ?>/assets/images/abdul_somad_300x300.jpg" alt="">						
					</div>
					<span class="title">Abdul Somad Lc. MA</span>
					<p>2 Kajian <br>
						<span class="utz-verified">Verified</span>
					</p>
					<a href="#" class="secondary-content-app waves-effect waves-light btn btn-small">Detail</a>
				</li>
				<li class="collection-item avatar">
					<div class="avatar-ustadz">
						<img src="<?php echo site_url('') ?>/assets/images/abdul_somad_300x300.jpg" alt="">						
					</div>
					<span class="title">Abdul Somad Lc. MA</span>
					<p>2 Kajian <br>
						<span class="utz-verified">Verified</span>
					</p>
					<a href="#" class="secondary-content-app waves-effect waves-light btn btn-small">Detail</a>
				</li>
				<li class="collection-item avatar">
					<div class="avatar-ustadz">
						<img src="<?php echo site_url('') ?>/assets/images/abdul_somad_300x300.jpg" alt="">						
					</div>
					<span class="title">Abdul Somad Lc. MA</span>
					<p>2 Kajian <br>
						<span class="utz-verified">Verified</span>
					</p>
					<a href="#" class="secondary-content-app waves-effect waves-light btn btn-small">Detail</a>
				</li>
				<li class="collection-item avatar">
					<div class="avatar-ustadz">
						<img src="<?php echo site_url('') ?>/assets/images/abdul_somad_300x300.jpg" alt="">						
					</div>
					<span class="title">Abdul Somad Lc. MA</span>
					<p>2 Kajian <br>
						<span class="utz-verified">Verified</span>
					</p>
					<a href="#" class="secondary-content-app waves-effect waves-light btn btn-small">Detail</a>
				</li>
			</ul>
		</div>

		<!-- Modal Search Ustadz -->
		<div id="search_ustadz" class="modal modal-app">
			<form class="col s12">
				<div class="modal-content">
					<h5>Cari Ustadz</h5>
					<div class="row">
						<div class="input-field col s12">
							<input id="search_ustadz" name="" type="text">
							<label for="search_ustadz">Cari Ustadz</label>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button class="btn waves-effect waves-light right" type="submit" name="action">Cari
						<i class="material-icons right">search</i>
					</button>
				</div>
			</form>
		</div>
	</div>
</div>