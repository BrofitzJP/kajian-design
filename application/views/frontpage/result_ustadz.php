<div class="title-page">1 Ustadz Ditemukan</div>

<!-- Content Page -->
<div class="container app-content">
	<div class="row">
		<div class="col s12">
			<!-- Top -->
			<div class="card-panel app-card teal">
				<table class="search-kajian">
					<tr>
						<td>Ustadz : Abdul Somad</td>
					</tr>
				</table>
			</div>

			<!-- List Ustadz -->
			<ul class="collection app-collection">
				<li class="collection-item avatar">
					<div class="avatar-ustadz">
						<img src="<?php echo site_url('') ?>/assets/images/abdul_somad_300x300.jpg" alt="">						
					</div>
					<span class="title">Abdul Somad Lc. MA</span>
					<p>2 Kajian <br>
						<span class="utz-verified">Verified</span>
					</p>
					<a href="#" class="secondary-content-app waves-effect waves-light btn btn-small">Detail</a>
				</li>
			</ul>

			<!-- Ustadz Suggested -->
			<p class="suggested"><em>Suggested</em></p>
			<ul class="collection app-collection">
				<li class="collection-item avatar">
					<div class="avatar-ustadz">
						<img src="<?php echo site_url('') ?>/assets/images/abdul_somad_300x300.jpg" alt="">						
					</div>
					<span class="title">Abdul Somad Lc. MA</span>
					<p>2 Kajian <br>
						<span class="utz-verified">Verified</span>
					</p>
					<a href="#" class="secondary-content-app waves-effect waves-light btn btn-small">Detail</a>
				</li>
				<li class="collection-item avatar">
					<div class="avatar-ustadz">
						<img src="<?php echo site_url('') ?>/assets/images/abdul_somad_300x300.jpg" alt="">						
					</div>
					<span class="title">Abdul Somad Lc. MA</span>
					<p>2 Kajian <br>
						<span class="utz-verified">Verified</span>
					</p>
					<a href="#" class="secondary-content-app waves-effect waves-light btn btn-small">Detail</a>
				</li>
				<li class="collection-item avatar">
					<div class="avatar-ustadz">
						<img src="<?php echo site_url('') ?>/assets/images/abdul_somad_300x300.jpg" alt="">						
					</div>
					<span class="title">Abdul Somad Lc. MA</span>
					<p>2 Kajian <br>
						<span class="utz-verified">Verified</span>
					</p>
					<a href="#" class="secondary-content-app waves-effect waves-light btn btn-small">Detail</a>
				</li>
				<li class="collection-item avatar">
					<div class="avatar-ustadz">
						<img src="<?php echo site_url('') ?>/assets/images/abdul_somad_300x300.jpg" alt="">						
					</div>
					<span class="title">Abdul Somad Lc. MA</span>
					<p>2 Kajian <br>
						<span class="utz-verified">Verified</span>
					</p>
					<a href="#" class="secondary-content-app waves-effect waves-light btn btn-small">Detail</a>
				</li>
			</ul>
		</div>
	</div>
</div>