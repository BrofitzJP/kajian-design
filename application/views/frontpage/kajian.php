<div class="top-page">
	<div class="title-page">Kajian</div>
	<a class="modal-trigger" href="#modal1"><i class="material-icons">search</i></a>
</div>

<!-- Content Page -->
<div class="container app-content">
	<div class="row">
		<div class="col s12">
			<!-- Kajian Card -->
			<div class="card kajian-card">
				<span class="title-card">Menuju Keluarga Bahagia Dunia Akhirat</span>
				<div class="card-image waves-effect waves-block waves-light">
					<img class="activator" src="<?php echo site_url('') ?>/assets/images/tree.jpg">
				</div>
				<div class="card-content">
					<div class="float-inner-card">
						<div class="people-img">
							<img src="<?php echo site_url('') ?>/assets/images/abdul_somad.jpg" alt="">
						</div>
						<div class="top-inner-card">
							<span class="utz-card">Abdul Somad Lc. MA</span>
						</div>
						<div class="bottom-inner-card">
							<span><i class="material-icons">date_range</i> Minggu, 02 Jan 2018</span>
							<span><i class="material-icons">alarm</i> 08.00 - 10.00 WIB</span>
						</div>
					</div>
					<div class="loc-card">
						<span><i class="material-icons">brightness_4</i> Masjid Al-Ikhlas</span>
						<span><i class="material-icons">place</i> Mojogedang RT03/RW 02 Kec. Mojogedang, Kab. Karanganyar, Prov. Jawa Tengah</span>
					</div>
				</div>
				<div class="card-footer">
					<span><i class="material-icons">account_circle</i> By Brofit J</span>
					<span class="activator right">Selengkapnya <i class="material-icons">keyboard_arrow_up</i></span>
				</div>
				<div class="card-reveal">
					<span class="card-title grey-text text-darken-4">Menuju Keluarga Bahagia Dunia Akhirat<i class="material-icons right">close</i></span>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
					<div class="footer-reveal">
						<em>Download file kajian</em>
						<button class="btn waves-effect waves-light">Download
							<i class="material-icons right">file_download</i>
						</button>
					</div>
					<div class="action-reveal text-center">
						<a href="<?php echo site_url() ?>/frontpage/edit_kajian" class="btn btn-small waves-effect waves-light red">
							<i class="material-icons">mode_edit</i>
						</a>
						<a href="#delete" class="btn btn-small waves-effect waves-light red modal-trigger">
							<i class="material-icons">delete_forever</i>
						</a>
					</div>
				</div>
			</div>
			<!-- Kajian Card -->
			<div class="card kajian-card">
				<span class="title-card">Menuju Keluarga Bahagia Dunia Akhirat</span>
				<div class="card-image waves-effect waves-block waves-light">
					<img class="activator" src="<?php echo site_url('') ?>/assets/images/tree.jpg">
				</div>
				<div class="card-content">
					<div class="float-inner-card">
						<div class="people-img">
							<img src="<?php echo site_url('') ?>/assets/images/abdul_somad.jpg" alt="">
						</div>
						<div class="top-inner-card">
							<span class="utz-card">Abdul Somad Lc. MA</span>
						</div>
						<div class="bottom-inner-card">
							<span><i class="material-icons">date_range</i> Minggu, 02 Jan 2018</span>
							<span><i class="material-icons">alarm</i> 08.00 - 10.00 WIB</span>
						</div>
					</div>
					<div class="loc-card">
						<span><i class="material-icons">brightness_4</i> Masjid Al-Ikhlas</span>
						<span><i class="material-icons">place</i> Mojogedang RT03/RW 02 Kec. Mojogedang, Kab. Karanganyar, Prov. Jawa Tengah</span>
					</div>
				</div>
				<div class="card-footer">
					<span><i class="material-icons">account_circle</i> By Brofit J</span>
					<span class="activator right">Selengkapnya <i class="material-icons">keyboard_arrow_up</i></span>
				</div>
				<div class="card-reveal">
					<span class="card-title grey-text text-darken-4">Menuju Keluarga Bahagia Dunia Akhirat<i class="material-icons right">close</i></span>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
					<div class="footer-reveal">
						<em>Download file kajian</em>
						<button class="btn waves-effect waves-light">Download
							<i class="material-icons right">file_download</i>
						</button>
					</div>
					<div class="action-reveal text-center">
						<a href="<?php echo site_url() ?>/frontpage/edit_kajian" class="btn btn-small waves-effect waves-light red">
							<i class="material-icons">mode_edit</i>
						</a>
						<a href="#delete" class="btn btn-small waves-effect waves-light red modal-trigger">
							<i class="material-icons">delete_forever</i>
						</a>
					</div>
				</div>
			</div>
			<!-- Kajian Card -->
			<div class="card kajian-card">
				<span class="title-card">Menuju Keluarga Bahagia Dunia Akhirat</span>
				<div class="card-image waves-effect waves-block waves-light">
					<img class="activator" src="<?php echo site_url('') ?>/assets/images/tree.jpg">
				</div>
				<div class="card-content">
					<div class="float-inner-card">
						<div class="people-img">
							<img src="<?php echo site_url('') ?>/assets/images/abdul_somad.jpg" alt="">
						</div>
						<div class="top-inner-card">
							<span class="utz-card">Abdul Somad Lc. MA</span>
						</div>
						<div class="bottom-inner-card">
							<span><i class="material-icons">date_range</i> Minggu, 02 Jan 2018</span>
							<span><i class="material-icons">alarm</i> 08.00 - 10.00 WIB</span>
						</div>
					</div>
					<div class="loc-card">
						<span><i class="material-icons">brightness_4</i> Masjid Al-Ikhlas</span>
						<span><i class="material-icons">place</i> Mojogedang RT03/RW 02 Kec. Mojogedang, Kab. Karanganyar, Prov. Jawa Tengah</span>
					</div>
				</div>
				<div class="card-footer">
					<span><i class="material-icons">account_circle</i> By Brofit J</span>
					<span class="activator right">Selengkapnya <i class="material-icons">keyboard_arrow_up</i></span>
				</div>
				<div class="card-reveal">
					<span class="card-title grey-text text-darken-4">Menuju Keluarga Bahagia Dunia Akhirat<i class="material-icons right">close</i></span>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
					<div class="footer-reveal">
						<em>Download file kajian</em>
						<button class="btn waves-effect waves-light">Download
							<i class="material-icons right">file_download</i>
						</button>
					</div>
					<div class="action-reveal text-center">
						<a href="<?php echo site_url() ?>/frontpage/edit_kajian" class="btn btn-small waves-effect waves-light red">
							<i class="material-icons">mode_edit</i>
						</a>
						<a href="#delete" class="btn btn-small waves-effect waves-light red modal-trigger">
							<i class="material-icons">delete_forever</i>
						</a>
					</div>
				</div>
			</div>
			<!-- Kajian Card -->
			<div class="card kajian-card">
				<span class="title-card">Menuju Keluarga Bahagia Dunia Akhirat</span>
				<div class="card-image waves-effect waves-block waves-light">
					<img class="activator" src="<?php echo site_url('') ?>/assets/images/tree.jpg">
				</div>
				<div class="card-content">
					<div class="float-inner-card">
						<div class="people-img">
							<img src="<?php echo site_url('') ?>/assets/images/abdul_somad.jpg" alt="">
						</div>
						<div class="top-inner-card">
							<span class="utz-card">Abdul Somad Lc. MA</span>
						</div>
						<div class="bottom-inner-card">
							<span><i class="material-icons">date_range</i> Minggu, 02 Jan 2018</span>
							<span><i class="material-icons">alarm</i> 08.00 - 10.00 WIB</span>
						</div>
					</div>
					<div class="loc-card">
						<span><i class="material-icons">brightness_4</i> Masjid Al-Ikhlas</span>
						<span><i class="material-icons">place</i> Mojogedang RT03/RW 02 Kec. Mojogedang, Kab. Karanganyar, Prov. Jawa Tengah</span>
					</div>
				</div>
				<div class="card-footer">
					<span><i class="material-icons">account_circle</i> By Brofit J</span>
					<span class="activator right">Selengkapnya <i class="material-icons">keyboard_arrow_up</i></span>
				</div>
				<div class="card-reveal">
					<span class="card-title grey-text text-darken-4">Menuju Keluarga Bahagia Dunia Akhirat<i class="material-icons right">close</i></span>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
					<div class="footer-reveal">
						<em>Download file kajian</em>
						<button class="btn waves-effect waves-light">Download
							<i class="material-icons right">file_download</i>
						</button>
					</div>
					<div class="action-reveal text-center">
						<a href="<?php echo site_url() ?>/frontpage/edit_kajian" class="btn btn-small waves-effect waves-light red">
							<i class="material-icons">mode_edit</i>
						</a>
						<a href="#delete" class="btn btn-small waves-effect waves-light red modal-trigger">
							<i class="material-icons">delete_forever</i>
						</a>
					</div>
				</div>
			</div>
			<!-- Kajian Card -->
			<div class="card kajian-card">
				<span class="title-card">Menuju Keluarga Bahagia Dunia Akhirat</span>
				<div class="card-image waves-effect waves-block waves-light">
					<img class="activator" src="<?php echo site_url('') ?>/assets/images/tree.jpg">
				</div>
				<div class="card-content">
					<div class="float-inner-card">
						<div class="people-img">
							<img src="<?php echo site_url('') ?>/assets/images/abdul_somad.jpg" alt="">
						</div>
						<div class="top-inner-card">
							<span class="utz-card">Abdul Somad Lc. MA</span>
						</div>
						<div class="bottom-inner-card">
							<span><i class="material-icons">date_range</i> Minggu, 02 Jan 2018</span>
							<span><i class="material-icons">alarm</i> 08.00 - 10.00 WIB</span>
						</div>
					</div>
					<div class="loc-card">
						<span><i class="material-icons">brightness_4</i> Masjid Al-Ikhlas</span>
						<span><i class="material-icons">place</i> Mojogedang RT03/RW 02 Kec. Mojogedang, Kab. Karanganyar, Prov. Jawa Tengah</span>
					</div>
				</div>
				<div class="card-footer">
					<span><i class="material-icons">account_circle</i> By Brofit J</span>
					<span class="activator right">Selengkapnya <i class="material-icons">keyboard_arrow_up</i></span>
				</div>
				<div class="card-reveal">
					<span class="card-title grey-text text-darken-4">Menuju Keluarga Bahagia Dunia Akhirat<i class="material-icons right">close</i></span>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
					<div class="footer-reveal">
						<em>Download file kajian</em>
						<button class="btn waves-effect waves-light">Download
							<i class="material-icons right">file_download</i>
						</button>
					</div>
					<div class="action-reveal text-center">
						<a href="<?php echo site_url() ?>/frontpage/edit_kajian" class="btn btn-small waves-effect waves-light red">
							<i class="material-icons">mode_edit</i>
						</a>
						<a href="#delete" class="btn btn-small waves-effect waves-light red modal-trigger">
							<i class="material-icons">delete_forever</i>
						</a>
					</div>
				</div>
			</div>
		</div>

		<!-- Modal Search Kajian -->
		<div id="modal1" class="modal modal-app">
			<form class="col s12">
				<div class="modal-content">
					<h5>Cari Kajian</h5>
					<div class="row">
						<div class="input-field col s12">
							<input id="icon_prefix" name="" type="text">
							<label for="icon_prefix">Tema Kajian</label>
						</div>
						<div class="input-field select2-no-prefix col s12">
							<select class="select2">
								<option value="" disabled selected>Pilih Provinsi</option>
								<option value="1">Banten</option>
								<option value="2">Jakarta</option>
								<option value="3">Jawa Barat</option>
								<option value="4">Jawa Tengah</option>
								<option value="5">Yogyakarta</option>
								<option value="6">Jawa Timur</option>
							</select>
							<label class="label-select2">Masjid</label>
						</div>
						<div class="input-field col s12">
							<input id="start-date" type="text" class="datepicker">
							<label for="start-date">Mulai Tanggal</label>
						</div>
						<div class="input-field col s12">
							<input id="end-date" type="text" class="datepicker">
							<label for="end-date">Sampai Tanggal</label>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button class="btn waves-effect waves-light right" type="submit" name="action">Cari
						<i class="material-icons right">search</i>
					</button>
				</div>
			</form>
		</div>

		<!-- Modal delete -->
		<div id="delete" class="modal">
			<div class="modal-content">
				<h5>Anda Yakin?</h5>
				<p>Apakah Anda yakin untuk menghapus kajian ini?</p>
			</div>
			<div class="modal-footer m-text-center">
				<a href="#!" class="waves-effect waves-light btn red btn-small">Hapus</a>
				<a href="#!" class="waves-effect waves-light btn btn-small modal-action modal-close">Batal</a>
			</div>
		</div>
	</div>
</div>