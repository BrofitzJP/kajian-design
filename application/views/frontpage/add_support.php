<div class="title-page">Add Support</div>

<!-- Content Page -->
<div class="container app-content">
	<div class="row">
		<div class="col s12">
			<div class="card-panel app-card">
				<div class="row">
					<!-- Add Kajian -->
					<form class="col s12">
						<div class="row">
							<div class="input-field col s12">
								<i class="material-icons prefix">title</i>
								<input id="title" name="" type="text" value="">
								<label for="title">Judul Inbox</label>
							</div>
							<div class="input-field col s12">
								<select>
									<option value="" disabled selected>Pillih Kategori</option>
									<option value="1">Saran</option>
									<option value="2">Bantuan</option>
								</select>
								<label>Pilih Kategori</label>
							</div>
							<div class="input-field col s12">
								<textarea id="messegge" class="materialize-textarea" data-length="520"></textarea>
								<label for="messegge">Tulis Pesan</label>
							</div>
							<div class="file-field input-field col s12">
								<div class="btn btn-small red">
									<i class="material-icons">attachment</i>
									<input type="file">
								</div>
								<div class="file-path-wrapper">
									<input class="file-path" type="text" placeholder="Unggah Lampiran">
								</div>
							</div>
						</div>
						<button class="btn waves-effect waves-light right" type="submit" name="action">Kirim
							<i class="material-icons right">send</i>
						</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>