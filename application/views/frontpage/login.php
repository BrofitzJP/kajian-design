<!doctype html>
<html lang="en">
	<head>
		<title>Login</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- CSS -->
		<link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
		<link rel="stylesheet" href="<?php echo site_url('') ?>assets/frameworks/materialize/css/materialize.min.css" crossorigin="anonymous">
		<link rel="stylesheet" href="<?php echo site_url('') ?>assets/css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body class="login">

		<!-- Login Page -->
		<div class="container">
			<div class="row">
				<div class="col s12">
					<div class="card-panel login-card">
						<div class="login-top-content">
							<h5><i class="material-icons">lock</i> Login</h5>
						</div>
						<form>
							<div class="row">
								<div class="input-field col s12">
									<input id="username" type="text" class="validate">
									<label for="username">Username or Email</label>
								</div>
								<div class="input-field col s12">
									<input id="password" type="tel" class="validate">
									<label for="password">Password</label>
								</div>
								<div class="col s7">
									<p>
										<input type="checkbox" class="filled-in" id="filled-in-box"/>
										<label for="filled-in-box">Remember Me</label>
									</p>
								</div>
								<div class="col s5">
									<a class="waves-effect waves-light btn btn-front right">Sign In</a>
								</div>
							</div>
							<div class="row login-bottom">
								<div class="col s6">
									<a href="<?php echo site_url() ?>/frontpage/register">Register Now</a>
								</div>
								<div class="col s6">
									<a href="<?php echo site_url() ?>/frontpage/forgot_password" class="right">Forgot Password?</a>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	
		<!-- JavaScript -->
		<script src="<?php echo site_url() ?>assets/js/jquery.min.js" crossorigin="anonymous"></script>
		<script src="<?php echo site_url() ?>assets/frameworks/materialize/js/materialize.min.js" crossorigin="anonymous"></script>
	</body>
</html>