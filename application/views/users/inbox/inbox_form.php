<?php echo validation_errors(); ?>
<?php
echo form_open_multipart( $send_mail_url, ['class' => 'std-form'] ); ?>
<div class="form-group">
    <label for="title">Judul :</label>
    <input type="text" name="title" value="<?php echo set_value('title');?>" class="form-control" />
</div>
<div class="form-group">
    <label for="category">Kategori :</label>
    <select name="category" id="category" class="form-control">
        <option value="">Select Category</option>
        <option value="saran" <?php echo set_select('category', 'saran');?>>Saran</option>
        <option value="report" <?php echo set_select('category', 'report');?>>Report</option>
    </select>
</div>
<div class="form-control">
    <label for="message">Messages</label>
    <textarea name="message" id="message" class="form-control" cols="30" rows="10"><?php set_value('message');?></textarea>
</div>
<div class="upload">
    <label for="">Upload</label>
    <input type="file" name="upload" />
</div>
<?php echo form_submit('submit', 'Tambahkan', 'class="btn btn-primary"');?>
</form>