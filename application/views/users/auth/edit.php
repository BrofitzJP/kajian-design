<?php echo validation_errors(); ?>
<?php
echo form_open( $actions, ['class' => 'std-form'] );
if ( isset($_GET['pass']) && $_GET['pass'] == 'not-match' ) {
    echo 'Password Not Match';
}
if ($users) :
foreach ((array) $users as $user) : ?>
    <input type="hidden" name="user_id" value="<?php echo $auth_user_id;?>">
<div class="form-group">
    <label for="name">Nama Lengkap</label>
    <input type="text" name="name" id="name" class="form-control form_input"
           value="<?php echo set_value('name', $user->display_name); ?>" autocomplete="off" maxlength="200" />
</div>
<div class="form-group">
    <label for="username">Username</label>
    <input type="text" name="username" id="username" disabled class="form-control form_input"
           value="<?php echo set_value('username', $user->username); ?>" autocomplete="off" maxlength="200" />
</div>
<div class="form-group">
    <label for="email">Email</label>
    <input type="text" name="email" id="email" class="form-control form_input"
           value="<?php echo set_value('email', $user->email); ?>" autocomplete="off" maxlength="200" />
</div>
<p>biarkan kosong jika tidak ingin ganti</p>
<div class="form-group">
    <label for="password">Password</label>
    <input type="password" name="password" id="password" class="form-control form_input password" <?php
    if( config_item('max_chars_for_password') > 0 )
        echo 'maxlength="' . config_item('max_chars_for_password') . '"';
    ?> autocomplete="off" readonly="readonly" onfocus="this.removeAttribute('readonly');" />
</div>
<div class="form-group">
    <label for="re-password">Re-Password</label>
    <input type="password" name="re-password" id="re-password" class="form-control form_input password" <?php
    if( config_item('max_chars_for_password') > 0 )
        echo 'maxlength="' . config_item('max_chars_for_password') . '"';
    ?> autocomplete="off" readonly="readonly" onfocus="this.removeAttribute('readonly');" />
</div>
    <hr>
<div class="form-group">
    <label for="date">Tanggal</label>
    <input type="date" name="date" id="date" class="form-control form_input" autocomplete="off"
           value="<?php echo set_value('date', date('Y-m-d', strtotime($user->born))); ?>"/>
</div>
<div class="form-group">
    <p>Jenis Kelamin</p>
    <label for="sex-l">Pria </label><input type="radio" name="sex" id="sex-l" class="form-control form_input" value="l"
        <?php echo $user->sex == 'l' ? 'checked' : '';?>/><br>
    <label for="sex-p">Wanita </label><input type="radio" name="sex" id="sex-p" class="form-control form_input" value="p"
        <?php echo $user->sex == 'p' ? 'checked' : '';?>/>
</div>
<div class="form-group">
    <label for="address">Alamat</label>
    <textarea name="address" id="address"
              class="form-control form-input"
              cols="30" rows="10"><?php echo $user->address;?></textarea>
</div>
<?php endforeach; else :
    echo 'No Data';
endif;?>
<input type="submit" name="submit" value="Submit" id="submit_button"  />
</form>
