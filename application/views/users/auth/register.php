<?php echo validation_errors(); ?>
<?php
echo form_open( $validation_url, ['class' => 'std-form'] ); ?>

<div class="form-group">
    <label for="name">Nama Lengkap</label>
    <input type="text" name="name" id="name" class="form-control form_input"
           value="<?php echo set_value('name'); ?>" autocomplete="off" maxlength="200" />
</div>
<div class="form-group">
    <label for="username">Username</label>
    <input type="text" name="username" id="username" class="form-control form_input"
           value="<?php echo set_value('username'); ?>" autocomplete="off" maxlength="200" />
</div>
<div class="form-group">
    <label for="email">Email</label>
    <input type="text" name="email" id="email" class="form-control form_input"
           value="<?php echo set_value('email'); ?>" autocomplete="off" maxlength="200" />
</div>
<div class="form-group">
    <label for="password">Password</label>
    <input type="password" name="password" id="password" class="form-control form_input password" <?php
    if( config_item('max_chars_for_password') > 0 )
        echo 'maxlength="' . config_item('max_chars_for_password') . '"';
    ?> autocomplete="off" readonly="readonly" onfocus="this.removeAttribute('readonly');" />
</div>
<div class="form-group">
    <label for="re-password">Re-Password</label>
    <input type="password" name="re-password" id="re-password" class="form-control form_input password" <?php
    if( config_item('max_chars_for_password') > 0 )
        echo 'maxlength="' . config_item('max_chars_for_password') . '"';
    ?> autocomplete="off" readonly="readonly" onfocus="this.removeAttribute('readonly');" />
</div>
<div class="form-group">
    <label for="date">Tanggal</label>
    <input type="date" name="date" id="date" class="form-control form_input" autocomplete="off"
           value="<?php echo set_value('date'); ?>"/>
</div>
<div class="form-group">
    <p>Jenis Kelamin</p>
    <label for="sex-l">Pria </label><input type="radio" name="sex" id="sex-l" class="form-control form_input" value="l"
        <?php echo set_radio('sex', 'l', true);?>/><br>
    <label for="sex-p">Wanita </label><input type="radio" name="sex" id="sex-p" class="form-control form_input" value="p"
        <?php echo set_radio('sex', 'p');?>/>
</div>
<div class="form-group">
    <label for="address">Alamat</label>
    <textarea name="address" id="address" class="form-control form-input" cols="30" rows="10">
        <?php echo set_value('address');?>
    </textarea>
</div>
<div class="form-group">
    <input type="checkbox" name="tos" id="tos" class="form-control form_input" value="tos-checked" />
    <label for="tos">Ya, Saya sudah membaca dan tunduk pada Term Of Service dari Situs Ini</label>
</div>
<input type="submit" name="submit" value="Submit" id="submit_button"  />

</form>