<?php

if( ! isset( $on_hold_message ) ) {
    if (isset($login_error_mesg)) {
        echo sprintf(
            "<div style=\"border:1px solid red;\"><p>%s</p><p>%s</p></div>",
            $this->authentication->login_errors_count . '/' . config_item('max_allowed_attempts'),
            'Login Error #%s: Invalid Username, Email Address, or Password.',
            'Username, email address and password are all case sensitive.'
        );
    }

    if ($this->input->get(AUTH_LOGOUT_PARAM)) {
        echo sprintf(
            "<div style=\"border:1px solid green\"><p>%s</p></div>",
            'You have successfully logged out.'
        );
    }
}

echo form_open( $login_url, ['class' => 'std-form'] );
?>

<div>

    <label for="login_string" class="form_label">Username or Email</label>
    <input type="text" name="login_string" id="login_string" class="form_input" autocomplete="off" maxlength="255" />

    <br />

    <label for="login_pass" class="form_label">Password</label>
    <input type="password" name="login_pass" id="login_pass" class="form_input password" <?php
    if( config_item('max_chars_for_password') > 0 )
        echo 'maxlength="' . config_item('max_chars_for_password') . '"';
    ?> autocomplete="off" readonly="readonly" onfocus="this.removeAttribute('readonly');" />


    <?php
    if( config_item('allow_remember_me') )
    {
        ?>

        <br />

        <label for="remember_me" class="form_label">Remember Me</label>
        <input type="checkbox" id="remember_me" name="remember_me" value="yes" />

        <?php
    }
    ?>

    <p>
        <?php
        $link_protocol = USE_SSL ? 'https' : NULL;
        ?>
        <a href="<?php echo site_url('user/forgot-password', $link_protocol); ?>">
            Can't access your account?
        </a>
    </p>
    <p>
        Belum punya Akun ? <a href="<?php echo site_url('user/register', $link_protocol); ?>">
            Daftar
        </a>
    </p>


    <input type="submit" name="submit" value="Login" id="submit_button"  />

</div>
</form>