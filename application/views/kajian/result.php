<?php
echo '<pre>';
print_r($filter);
echo '</pre>';

$currentUrl = $this->input->get('s') ? '?s=' . $this->input->get('s') : '';
$currentUrl = site_url($this->uri->uri_string() . $currentUrl);

foreach ($filter as $item => $value) {
    if (empty($value)) {
        continue;
    }

    echo '<h3>' . ucfirst($item) . '</h3>';
    switch ($item) {
        case 'location':
            foreach ($value as $name => $num) {
                foreach ($num as $id => $count) {
                    $currentUrl .= '&'.$name.'='.$id ;
                    echo sprintf('<p>%s : (%s)</p>',
                        anchor($currentUrl, get_name_location_id($id, $name)),
                        $count
                    );
                }
            }
            break;
        case 'days':
            foreach ($value as $name => $num) {
                echo '<p>' . convert_day($name) . ' : (' .$num .')</p>';
            }
            break;
        case 'mosques':
            foreach ($value as $id => $num) {
                echo '<p>' . get_masjid_term($id) . ' : (' .$num .')</p>';
            }
            break;
        case 'ustadz':
            foreach ($value as $name => $num) {
                echo '<p>' . get_ustadz_term($name) . ' : (' .$num .')</p>';
            }
            break;
    }
}
//echo '<hr/>' . $this->uri->uri_string() . $_SERVER['QUERY_STRING'];
//print_r($results);