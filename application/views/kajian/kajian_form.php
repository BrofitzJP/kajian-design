<?php echo validation_errors();?>
<?php echo form_open_multipart($actions);?>
<div class="form-group">
    <label for="tema">Tema Kajian</label>
    <input type="text" name="tema" class="form-control"
           value="<?php echo set_value('tema');?>" autocomplete="off" id="tema" />
</div>
<div class="form-group">
    <label for="ustadz">Nama Ustadz</label>
    <select name="ustadz" id="ustadz">
        <option value="">Pilih Ustadz</option>
        <?php
        /**
         * option ustadz name
         */
         if (isset($ustadz) && is_array($ustadz)) :

             foreach ($ustadz as $item) :

                echo sprintf(
                    '<option value="%s" %s>%s</option>',
                    $item->id_ustadz,
                    set_select('ustadz', $item->id_ustadz),
                    $item->ustadz_name
                );

             endforeach;

         endif; ?>
    </select>
</div>

<div class="form-group">
    <label for="masjid">Masjid</label>
    <select name="masjid" id="masjid">
        <option value="">Pilih Masjid</option>
        <?php
        /**
         * option ustadz name
         */
        if (isset($masjid) && is_array($masjid)) :

            if (isset($item)) {
                unset($item);
            }

            foreach ($masjid as $item) :

                echo sprintf(
                    '<option value="%s" %s>%s</option>',
                    $item->id_mosque,
                    set_select('masjid', $item->id_mosque),
                    $item->mosque_name
                );

            endforeach;

        endif; ?>
    </select>
</div>
<div class="form-group">
    <label for="date">Hari dan Tanggal</label>
    <input type="date" class="form-control" name="date" id="date"
    <?php echo set_value('date');?> />
</div>
<div class="form-group">
    <label for="begin">Jam Mulai</label>
    <input type="begin" class="form-control" name="begin" id="begin"
        <?php echo set_value('begin');?> />
</div>
<div class="form-group">
    <label for="end">Jam Selesai</label>
    <input type="end" class="form-control" name="end" id="end"
        <?php echo set_value('end');?> />
</div>
<button class="btn" type="submit">Tambah</button>
</form>
