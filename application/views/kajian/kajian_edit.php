<?php
if ($error) {
    show_error('Cannot Access', 200, 'ERROR PERMISSION');
}

if (isset($_GET['update']) && $_GET['update'] == 'success') {
    echo 'update sukses';
}
?>
<?php echo validation_errors();?>
<?php echo form_open_multipart($actions);?>
<?php if ($result) :
          foreach ($result as $row) : ?>
<div class="form-group">
    <input type="hidden" name="kajian_id" value="<?php echo $row->kajian_id;?>">
    <label for="tema">Tema Kajian</label>
    <input type="text" name="tema" class="form-control"
           value="<?php echo set_value('tema', $row->tema);?>" id="tema" />
</div>

<div class="form-group">
    <label for="ustadz">Nama Ustadz</label>
    <input type="hidden" class="ustadz_id" name="ustadz_id" value="<?php echo $row->ustadz_id;?>">
    <input type="text" name="ustadz" class="ustadz_name" autocomplete="off" value="<?php echo $row->ustadz_name;?>">
    <ul class="option-select-ustadz"></ul>
</div>

<div class="form-group">
    <label for="masjid">Masjid</label>
    <input type="hidden" name="masjid_id" class="masjid_id" value="<?php echo $row->location;?>">
    <input type="text" name="masjid" autocomplete="off" class="masjid_name" value="<?php echo $row->mosque_name;?>">
    <ul class="option-select-masjid"></ul>
</div>

<div class="form-group">
    <label for="date">Hari dan Tanggal</label>
    <input type="date" class="form-control" name="date" id="date"
        <?php echo set_value('date');?> value="<?php echo date('Y-m-d', strtotime($row->date_time));?>"/>
</div>
<div class="form-group">
    <label for="text">Jam Mulai</label>
    <input type="begin" class="form-control" name="begin" id="begin"
        <?php echo set_value('begin');?> value="<?php echo $row->begin_time;?>" />
</div>
<div class="form-group">
    <label for="end">Jam Selesai</label>
    <input type="text" class="form-control" name="end" id="end"
        <?php echo set_value('end', $row->end_time);?> value="<?php echo $row->end_time;?>"/>
</div>
<?php
endforeach; endif;
?>
<button class="btn" type="submit">Simpan</button>
</form>

<script src="<?php echo base_url('assets/js/jquery.min.js');?>"></script>
<script>
    $(document).ready(function() {
        /**
         * Ajax Ustadz
         */
        $('.ustadz_name').on('keyup', function (e) {
            e.preventDefault();
            var Val = $(this).val();
            Val = Val.replace(/\s+/, ' ').replace(/^\s+/, '').replace(/\s+$/);
            var list = '';
            var optionSelect = $('.option-select-ustadz');
            if (Val.length > 2) {
                $.ajax({
                    type: 'GET',
                    cache: false,
                    url: '<?php echo site_url('ustadz/get-ustadz', $ssl);?>',
                    dataType: 'json',
                    data: {
                        'keyword': Val
                    },
                    success: function (options) {
                        if (typeof options.error === "undefined") {
                            $.each(options, function (index, val) {
                                list += '<li data-id="' +val.id_ustadz+ '">' + val.ustadz_name + '</li>';
                            });
                        } else {
                            list = ('<li class="error">' + options.error + '</li>');
                        }

                        optionSelect.html(list);
                    }
                });
            } else {
                return;
            }
        });

        // click option select
        $('.option-select-ustadz').on('click', 'li', function (e) {
            e.preventDefault();
            var id_location = $(this).data('id');
            $('.ustadz_id').val(id_location);
            $('.ustadz_name').val($(this).text());
            $(this).parent().find('li').remove();
        });

        $('.masjid_name').on('keyup', function (e) {
            e.preventDefault();
            var Val = $(this).val();
            Val = Val.replace(/\s+/, ' ').replace(/^\s+/, '').replace(/\s+$/);
            var list = '';
            var optionSelect = $('.option-select-masjid');
            if (Val.length > 2) {
                $.ajax({
                    type: 'GET',
                    cache: false,
                    url: '<?php echo site_url('masjid/get-masjidjson', $ssl);?>',
                    dataType: 'json',
                    data: {
                        'keyword': Val
                    },
                    success: function (options) {
                        console.log(options);
                        if (typeof options.error === "undefined") {
                            $.each(options, function (index, val) {
                                list += '<li data-id="' +val.id_mosque+ '"><p class="mosque_name">' + val.mosque_name + '<p>'
                                    +'<p><small>'+ val.propinsi +' > ' +val.kabkota+ ' > ' + val.kecamatan + ' > ' + val.kelurahan + '</small></p>'
                                    +'</li>';
                            });
                        } else {
                            list = ('<li class="error">' + options.error + '</li>');
                        }

                        optionSelect.html(list);
                    }
                });
            } else {
                return;
            }
        });

        // click option select
        $('.option-select-masjid').on('click', 'li', function (e) {
            e.preventDefault();
            var id_location = $(this).data('id');
            $('.masjid_id').val(id_location);
            $('.masjid_name').val($(this).find('.mosque_name').text());
            $(this).parent().find('li').remove();
        });

    });
</script>