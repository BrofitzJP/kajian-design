<?php
/**
 * Keyword (default)
 * dari Tanggal Sampai Tanggal (opsional)
 *
 * FILTER
 * by Propinsi
 *    Kab/Kota
 *      Kecamatan
 *        Kelurahan
 * by Masjid
 * by Hari
 *
 * LIMITASI current date - last day in current year (31 Desember) (default)
 */
echo form_open($search, ['method' => 'get']); ?>
<div class="form-group">
    <label for="">Cari</label>
    <input type="text" name="s">
</div>
<br>
<p>Optional</p>
<div class="form-group">
    <label for="">Propinsi</label>
    <select name="pro" id="">
        <option value="">Pilih Propinsi</option>
        <?php if ($propinsi) :
            foreach ((array) $propinsi as $pro) :
                printf('<option value="%s">%s</option>',
                    $pro->id,
                    $pro->name
                );
            endforeach;
        endif; ?>
    </select>
</div>
<div class="form-group">
    <label for="">Mulai Tanggal</label>
    <input type="date" name="start">
    <label for="">Sampai Tanggal</label>
    <input type="date" name="end">
</div>
<br>
<button type="submit">Cari</button>
</form>

<?php
if ($error) {
    echo $error_message;
} else {
    if ($results) :
        foreach ($results as $row) : ?>
        <div class="posts">
            <h3 class="title-post"><?php echo anchor(site_url('kajian/p/'. $row->kajian_id, $ssl), $row->tema);?></h3>
            <div class="pengisi">Pengisi : <?php echo anchor(site_url('ustadz/id/' . $row->ustadz_id, $ssl), $row->ustadz_name);?></div>
            <div class="lokasi">
                <p>Masjid Nama : <?php echo $row->mosque_name;?></p>
                <?php echo get_masjid_location($row->mosque_location);?>
            </div>
            <div class="edit">
                <?php share_button( site_url('kajian/p/'. $row->kajian_id, $ssl) ) ;?>
                <?php if ($row->user_id == $auth_user_id) :?>
                    <?php echo anchor(site_url('kajian/edit/'. $row->kajian_id, $ssl), 'Edit');?>
                    <?php echo anchor(site_url('kajian/delete/'. $row->kajian_id, $ssl), 'Delete');?>
                <?php endif;?>
            </div>
        </div>
        <?php endforeach;
    endif;
}
?>

<?php echo $add;?>