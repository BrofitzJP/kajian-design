<?php echo validation_errors(); ?>
<?php
echo form_open( $actions, ['class' => 'std-form'] );

if (! $results) :
    echo 'Not Found';
else :
    foreach ($results as $row) : ?>

<div class="form-group">
    <input type="hidden" value="<?php echo $row->id_ustadz;?>">
    <label for="name">Nama Ustadz</label>
    <input type="text" class="form-control" name="name_ustadz"
           value="<?php echo set_value('name_ustadz', $row->ustadz_name);?>" placeholder="Nama Ustadz"/>
</div>
<div class="form-group">
    <label for="study">Pendidikan Terakhir</label>
    <input type="text" class="form-control" name="study_ustadz"
           value="<?php echo set_value('study_ustadz', $row->pendidikan);?>" placeholder="S1 Al Azhar"/>
</div>
<div class="form-group">
    <label for="job">Jabatan di ORMAS</label>
    <input type="text" class="form-control" name="job_ustadz"
           value="<?php echo set_value('job_ustadz', $row->jabatan);?>" placeholder="Penasehat"/>
</div>
<?php
    endforeach;
endif;
?>
<button class="btn" type="submit">Update</button>
</form>
