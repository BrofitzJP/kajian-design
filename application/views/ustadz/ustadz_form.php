<?php echo validation_errors(); ?>
<?php
echo form_open( $add_new_ustadz, ['class' => 'std-form'] ); ?>

<div class="form-group">
    <label for="name">Nama Ustadz</label>
    <input type="text" class="form-control" name="name_ustadz"
           value="<?php echo set_value('name_ustadz');?>" placeholder="Nama Ustadz"/>
</div>
<div class="form-group">
    <label for="study">Pendidikan Terakhir</label>
    <input type="text" class="form-control" name="study_ustadz"
           value="<?php echo set_value('study_ustadz');?>" placeholder="S1 Al Azhar"/>
</div>
<div class="form-group">
    <label for="job">Jabatan di ORMAS</label>
    <input type="text" class="form-control" name="job_ustadz"
           value="<?php echo set_value('job_ustadz');?>" placeholder="Penasehat"/>
</div>
<button class="btn" type="submit">Tambah</button>
</form>
