<?php
echo validation_errors();
echo form_open_multipart($actions);?>
<div class="form-group">
    <label for="photo_masjid">Foto Masjid</label>
    <input type="file" name="photo_masjid" value="<?php echo set_value('photo_masjid');?>" class="form-control"/>
</div>
<div class="form-group">
    <label for="name_masjid">Nama Masjid</label>
    <input type="text" name="name_masjid" value="<?php echo set_value('name_masjid');?>" class="form-control"/>
</div>


<p>Location :</p>
<div class="form-group">
    <label for="">Propinsi</label>
    <select name="propinsi" id="propinsi">
        <option value="">Pilih Propinsi</option>
        <?php foreach ((array) $provinces as $pro) {
    echo sprintf('<option value="%s" %s>%s</option>',
        $pro->id,
        ($pro->id == $r->location_full->propinsi_id ? 'selected' : ''),
        $pro->name
    );
} ?>
</select>
</div>

<div class="form-group">
    <label for="">Kabupaten / Kota</label>
    <select name="kabkota" id="kabkota">
        <option value="">Pilih Kabupaten Kota</option>
    </select>
</div>

<div class="form-group">
    <label for="">Kecamatan</label>
    <select name="kec" id="kecamatan">
        <option value="">Pilih Kecamatan</option>
    </select>
</div>

<div class="form-group">
    <label for="">Kelurahan</label>
    <select name="kelurahan" id="kelurahan">
        <option value="">Pilih Kelurahan</option>
    </select>
</div>

<div class="form-group">
    <label for="address">Alamat Masjid</label>
    <textarea name="address" id="address" cols="30" rows="10"><?php echo set_value('address');?></textarea>
</div>
<div class="form-group">
    <label for="takmir">Nama Takmir</label>
    <input type="text" name="takmir" value="<?php echo set_value('takmir');?>" class="form-control"/>
</div>
<div class="form-group">
    <label for="no_takmir">Kontak Takmir</label>
    <input type="text" name="no_takmir" value="<?php echo set_value('no_takmir');?>" class="form-control"/>
</div>
<div class="form-group">
    <label for="no_takmir_ii">Kontak Takmir II</label>
    <input type="text" name="no_takmir_ii" value="<?php echo set_value('no_takmir_ii');?>" class="form-control"/>
</div>
<div class="form-group">
    <label for="email">Email</label>
    <input type="text" name="email" value="<?php echo set_value('email');?>" class="form-control"/>
</div>
<div class="form-group">
    <label for="jumlah">Jumlah Jamaah Subuh</label>
    <input type="number" name="jumlah" value="<?php echo set_value('jumlah');?>" class="form-control"/>
</div>
<div class="form-group">
    <label for="map">Google Maps</label>
    <textarea name="map" id="map" cols="30" rows="10"><?php echo set_value('map');?></textarea>
</div>
<button class="btn" type="submit">Tambah</button>
</form>

<?php on_footer();?>