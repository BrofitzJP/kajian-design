<?php
echo form_open_multipart($actions);
if ($masjid) :
    foreach ($masjid as $r) :
        $loc = $r->location_full; ?>
<div class="form-group">
    <label for="">Upload : </label>
    <input type="file" name="photo_masjid">
    <?php echo get_thumbnail($r->attachment);?>
</div>
<div class="form-group">
    <label for="">Nama Masjid</label>
    <input type="text" name="name_masjid" value="<?php echo $r->mosque_name;?>">
</div>
<p>Location :</p>
<div class="form-group">
    <label for="">Propinsi</label>
    <select name="propinsi" id="propinsi">
        <option value="">Pilih Propinsi</option>
        <?php foreach ((array) $provinces as $pro) {
            echo sprintf('<option value="%s" %s>%s</option>',
                $pro->id,
                ($pro->id == $loc->propinsi_id ? 'selected' : ''),
                $pro->name
            );
        } ?>
    </select>
</div>

<div class="form-group">
    <label for="">Kabupaten / Kota</label>
    <select name="kabkota" id="kabkota">
        <option value="">Pilih Kabupaten Kota</option>
        <?php
        $kabkotas = get_kabupatenkota($r->location_full->propinsi_id);
        foreach((array)$kabkotas as $kk) {
            echo sprintf('<option value="%s" %s>%s</option>',
                $kk->id,
                ($kk->id == $loc->kabupatenkota_id ? 'selected' : ''),
                $kk->name
            );
        }
        ?>
    </select>
</div>

<div class="form-group">
    <label for="">Kecamatan</label>
    <select name="kec" id="kecamatan">
        <option value="">Pilih Kecamatan</option>
        <?php
        $kecamatan_id = '';
        if (isset($loc->kecamatan_id)) {
            $kecamatan_id = $loc->kecamatan_id;
        }
        $kecamatans = get_kecamatan($r->location_full->kabupatenkota_id);
        foreach ((array)$kecamatans as $kec) {
            echo sprintf('<option value="%s" %s>%s</option>',
                $kec->id,
                ($kec->id == $kecamatan_id ? 'selected' : ''),
                $kec->name
            );
        }

        ?>
    </select>
</div>

<div class="form-group">
    <label for="">Kelurahan</label>
    <select name="kelurahan" id="kelurahan">
        <option value="">Pilih Kelurahan</option>
        <?php
        $id_desa = '';
        if (isset($loc->desa_id)) {
            $id_desa = $loc->desa_id;
        }
        $kelurahans = get_kelurahan($loc->kecamatan_id);
        foreach ((array)$kelurahans as $kel) {
            echo sprintf('<option value="%s" %s>%s</option>',
                $kel->id,
                ($kel->id == $id_desa ? 'selected' : ''),
                $kel->name
            );
        }

        ?>
    </select>
</div>

<p>Alamat</p>
<div class="form-group">
    <textarea name="address" id="" cols="30" rows="10"><?php echo $r->address;?></textarea>
</div>

<p>Maps</p>
<div class="form-group">
    <textarea name="map" id="" cols="30" rows="10"><?php echo html_entity_decode($r->maps);?></textarea>
</div>

<div class="form-group">
    <label for="">Jamaah Subuh</label>
    <input type="text" name="jumlah" value="<?php echo $r->jamaah;?>">
</div>


<div class="form-group">
    <label for="">Takmir Masjid</label>
    <input type="text" name="takmir" value="<?php echo $r->takmir;?>">
</div>

<div class="form-group">
    <label for="">Takmir Telp.</label>
    <input type="text" name="no_takmir" value="<?php echo $r->telp_takmir;?>">
</div>
<div class="form-group">
    <label for="">Takmir Telp. 2</label>
    <input type="text" name="no_takmir_ii" value="<?php echo $r->telp_takmir_other;?>">
</div>

<p>Manajer</p>
<?php
    $admin  = '';
    $editor = '';
    foreach ((array) $r->manager as $m) {
        if ($m->role == '9') {
            $admin .= '<li>' . $m->display_name . '</li>';
        } else {
            $editor .= '<li>' . $m->display_name . '</li>';
        }
    }

    echo '<ul>' . $admin . '</ul>';
    echo '<ul>' . $editor . '</ul>';
endforeach;
 if ($admin_role == '9') {
     echo anchor(site_url('masjid/add-admin/' . $r->id_mosque), 'Edit Manager');
 }
endif;?>

<button type="submit">Simpan</button>
</form>

<?php on_footer();?>