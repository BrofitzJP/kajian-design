<?php
if ($role != '9') {
    show_error('Cannot Access', 200, 'ERROR PERMISSION');
}
echo get_masjid_location($masjid->maps);
echo get_thumbnail($masjid->attachment);

if (isset($masjid->manager)) {
    echo '<hr/>';
    foreach ((array) $masjid->manager as $item) {
        echo get_user_by_id($item->user_id);
        echo anchor(site_url('masjid/delete-manager/' . $masjid->id_mosque . '/' . $item->user_id), 'delete');
        echo '<br/>';
    }
}

echo validation_errors();
echo form_open($action); ?>
<input type="hidden" name="mosque_id" value="<?php echo $masjid->id_mosque;?>">
<?php for ($i = 0; $i < 3; $i++) :?>
<select name="editor[]" id="editor">
    <option value="">Pilih Editor</option>
    <?php
    if ($free_users) {
        foreach ((array) $free_users as $user) {
            echo sprintf('<option value="%s">%s</option>',
                $user->user_id,
                $user->display_name
            );
        }
    }
    ?>
</select>
<select name="user_role[]" id="user_role">
    <option value="">Pilih Hak Akses</option>
    <option value="9">Admin</option>
    <option value="1">Editor</option>
</select>
<br>
<?php endfor;?>
<button type="submit">Simpan</button>
</form>
