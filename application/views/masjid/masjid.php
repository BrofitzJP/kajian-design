<?php
if ($masjid) {
    foreach ($masjid as $row) : ?>
        <h2><?php echo $row->mosque_name;?></h2>
        <p>Lokasi : <?php
            $location = $row->location_full;

            $lokasi = '';
            if (isset($location->propinsi)) {
                $lokasi .= $location->propinsi;
                $lokasi .= ', ';
            }

            if (isset($location->kabupatenkota)) {
                $lokasi .= $location->kabupatenkota;
                $lokasi .= ', ';
            }

            if (isset($location->kecamatan)) {
                $lokasi .= $location->kecamatan;
                $lokasi .= ', ';
            }

            if (isset($location->desa)) {
                $lokasi .= $location->desa;
                $lokasi .= ', ';
            }

            echo trim($lokasi, ', ');
            ?></p>
        <p>Alamat : <?php echo $row->address;?></p>
        <ul>
            <?php
            if (is_admin_masjid($row->mosque_id)) : ?>
                <li><?php echo anchor(site_url('masjid/edit/' . $row->mosque_id, $use_ssl), 'Edit');?></li>
                <?php if (is_admin_masjid($row->mosque_id) == '9') : ?>
                    <li><?php echo anchor(site_url('masjid/d/' . $row->mosque_id, $use_ssl), 'Add Editor');?></li>
                <?php endif; ?>
            <?php endif; ?>
        </ul>
    <?php
    endforeach;
}

echo $add;