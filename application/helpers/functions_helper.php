<?php
/**
 * Option Setting Handler
 */
if ( ! function_exists('get_option') ) {
    function get_option($name, $default = false)
    {
        static $options;
        if ( empty($options) ) {
            $CI =& get_instance();
            $query = $CI->db->get('options');
            $options = $query->result_array();
        }

        foreach ($options as $option) {
            if ( $option['option_name'] == $name ) {
                return $option['option_value'];
                break;
            }
        }

        return $default;
    }
}

/**
 * Site Handler
 */
if (! function_exists('to_login')) {
    function to_login()
    {
        $CI =& get_instance();
        redirect(site_url('user', USE_SSL ? 'https' : NULL));
    }
}

/**
 * Navigation Handle
 */
function primary_navigation()
{
    $CI =& get_instance();

    $format = '<li><a href="%s">%s</a></li>';
    $use_ssl = USE_SSL ? 'https' : NULL;
    echo '<ul>';

    echo sprintf($format,
        site_url('@' . config_item('auth_username'), $use_ssl),
        'Profile'
    );

    echo sprintf($format,
        site_url('inbox', $use_ssl),
        'Inbox'
    );

    echo sprintf($format,
        site_url('ustadz', $use_ssl),
        'Ustadz'
    );

    echo sprintf($format,
        site_url('kajian', $use_ssl),
        'Kajian'
    );

    echo sprintf($format,
        site_url('masjid', $use_ssl),
        'Masjid'
    );

    echo sprintf(
        $format,
        site_url('user/logout', $use_ssl),
        'Logout'
    );

    echo '</ul>';
}

function on_head()
{}

function on_footer()
{
    $use_ssl = USE_SSL ? 'https' : NULL;
    ?>
    <script src="<?php echo base_url('assets/js/jquery.min.js', $use_ssl);?>"></script>
    <script src="<?php echo base_url('assets/js/scripts.js', $use_ssl);?>"></script>
    <script>
        $(document).ready(function() {
            // #kabkota
            $('#propinsi').on('change', function (e) {
                e.preventDefault();
                var thisVal = $(this).val();
                var target  = $('#kabkota');

                send_ajax('<?php echo site_url('masjid/get-kabupaten', $use_ssl);?>', {'id_propinsi' : thisVal}, target);

            });

            $('#kabkota').on('change', function (e) {
                e.preventDefault();
                var thisVal = $(this).val();
                var target  = $('#kecamatan');

                send_ajax('<?php echo site_url('masjid/get-kecamatan', $use_ssl);?>', {'id_kabupaten' : thisVal}, target);
            });

            $('#kecamatan').on('change', function (e) {
                e.preventDefault();
                var thisVal = $(this).val();
                var target  = $('#kelurahan');

                send_ajax('<?php echo site_url('masjid/get-kelurahan', $use_ssl);?>', {'id_kecamatan' : thisVal}, target);
            });

        });
    </script>
    <?php
}

if ( ! function_exists('native_mail') ) {
    function native_mail($args = [])
    {
        $default = [
            'to' => 'Mary <mary@example.com>, Kelly <kelly@example.com>',
            'from' => 'Birthday Reminder <birthday@example.com>',
            'cc'  => '',
            'bcc' => '',
            'subject' => 'Notifcation',
        ];

        $args = array_merge($default, $args);

        // To send HTML mail, the Content-type header must be set
        $headers[] = 'MIME-Version: 1.0';
        $headers[] = 'Content-type: text/html; charset=utf-8';

        // Additional headers
        $headers[] = 'To: ' . $args['to'];
        $headers[] = 'From: ' . $args['from'];

        if ($args['cc'] !== '') {
            $headers[] = 'Cc: ' . $args['cc'];
        }

        if ($args['bcc'] !== '') {
            $headers[] = 'Bcc: ' . $args['bcc'];
        }

        ob_start();

        /**
         * Call / require file template 
         * require '/config/email/email.php';
         */

        $message = ob_get_clean();

        mail('', $args['subject'], $message, implode("\r\n", $headers));
    }

}

/**
 * Email Handler
 */
if ( ! function_exists('send_mail')) {
    function send_mail($args = array())
    {
        $default = array(
            'email_as' => 'Kajian Dev',
            'subject' => 'Subject Email',
            'to'      => '',
            'from'    => 'noreply-kajian@kajian.com',
            'reply'   => 'noreply-kajian@kajian.com',
            'message' => '<p>Sample Content</p>',
        );

        $CI =& get_instance();
        $CI->load->library('email');

        $args = array_merge($default, $args);

        $subject = $args['subject'];
        $message = $args['message'];

        $body = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=' . strtolower(config_item('charset')) . '" />
    <title>' . html_escape($subject) . '</title>
    <style type="text/css">
        body {
            font-family: Arial, Verdana, Helvetica, sans-serif;
            font-size: 16px;
        }
    </style>
</head>
<body>
' . $message . '
</body>
</html>';

        // Also, for getting full html you may use the following internal method:
        $body = $CI->email->full_html($subject, $message);

        $result = $CI->email
            ->from($args['from'], $args['email_as'])
            ->reply_to($args['reply'])    // Optional, an account where a human being reads.
            ->to($args['to'])
            ->subject($subject)
            ->message($body)
            ->send();

        if ( ! $result ) {
            echo $CI->email->print_debugger();
            exit();
        }
    }
}

/**
 * Share Button
 */
if ( ! function_exists('share_button')) {
    function share_button($link)
    {
        $links = [
           'telegram' => 'https://t.me/share/url?url=' . rawurlencode($link),
           'facebook' => 'https://www.facebook.com/sharer/sharer.php?u=' . rawurlencode($link),
           'twitter'  => 'https://twitter.com/home?status=' . rawurlencode($link),
           'whatsapp' => 'https://api.whatsapp.com/send?text=' . rawurlencode($link)
        ];

        foreach ($links as $key => $link) {
            echo anchor($link, $key, 'target = "_blank"');
            echo ', ';
        }
    }
}


/**
 * restore htmlentities iframe
 */
function to_html($string)
{
    $html = html_entity_decode($string);
    $html = str_replace('&lt;', '<', $html);
    $html = str_replace('&gt;', '>', $html);

    return $html;
}

/**
 * GET DAY FROM DATETIME
 * @param $datetime
 * @return mixed
 */
function get_day($datetime)
{
    $day = date('D', strtotime($datetime));
    return convert_day($day);
}

/**
 * EN to ID date
 * @param $name
 * @return bool|mixed
 */
function convert_day($name)
{
    $days = [
        'Sun' => 'Minggu',
        'Mon' => 'Senin',
        'Tue' => 'Selasa',
        'Wed' => 'Rabu',
        'Thu' => 'Kamis',
        'Fri' => 'Jumat',
        'Sat' => 'Sabtu'
    ];

    if (isset($days[$name])) {
        return $days[$name];
    }

    return false;
}

/**
 * @param $mosque_id
 * @param string $term
 * @return string
 */
function get_masjid_term($mosque_id, $term = 'mosque_name')
{
    $CI =& get_instance();
    $CI->db->select($term);
    $query = $CI->db->get_where('mosque', [
        'mosque_id' => $mosque_id
    ]);

    $result = false;
    if ($query->num_rows() > 0) {
        $result = $query->result()[0]->$term;
    }

    $CI->db->reset_query();
    return $result;
}

/**
 * Display information location of masjid
 * @param $location
 * @return string
 */
function get_masjid_location($location)
{
    $CI =& get_instance();

    $location = $CI->general->get_masjid([
        'where' => [
            [
                'field' => 'location',
                'value' => $location
            ],
        ]
    ]);

    $format = '<li>%s : %s</li>';
    $render = '<ul>';
    foreach($location as $item) {
        $loc = $item->location_full;

        if ( isset($loc->propinsi) ) {
            $render .= sprintf($format, 'Propinsi' ,ucfirst($loc->propinsi));

            if (isset($loc->kabupatenkota)) {
                $render .= sprintf($format, 'Kabupaten/Kota' ,ucfirst($loc->kabupatenkota));

                if (isset($loc->kecamatan)) {
                    $render .= sprintf($format, 'Kecamatan' ,ucfirst($loc->kecamatan));

                    if (isset($loc->desa)) {
                        $render .= sprintf($format, 'Kelurahan' ,ucfirst($loc->desa));
                    }
                }
            }
        }
    }

    return $render . '</ul>';
}

/**
 * @param $location_id
 * @param $location
 * @return mixed
 */
function get_name_location_id($location_id, $location)
{
    $CI =& get_instance();

    $CI->db->select('name');

    $loc = '';
    switch ($location) {
        case 'pro':
            $pro = $CI->db->get_where('provinces', [
                'id' => $location_id
            ]);

            if ($pro->num_rows() == 1) {
                $loc = $pro->result()[0]->name;
            }

            break;
        case 'kabkota':
            $kabkota = $CI->db->get_where('regencies', [
                'id' => $location_id
            ]);

            if ($kabkota->num_rows() == 1) {
                $loc = $kabkota->result()[0]->name;
            }

            break;
        case 'kec':
            $kec = $CI->db->get_where('districts', [
                'id' => $location_id
            ]);

            if ($kec->num_rows() == 1) {
                $loc = $kec->result()[0]->name;
            }

            break;
        case 'kel':
            $kel = $CI->db->get_where('villages', [
                'id' => $location_id
            ]);

            if ($kel->num_rows() == 1) {
                $loc = $kel->result()[0]->name;
            }
            break;
    }
    $CI->db->reset_query();
    return $loc;
}
/**
 * @param $propinsi_id
 * @return array
 */
function get_kabupatenkota($propinsi_id)
{
    $CI =& get_instance();
    $kabkota = $CI->db->get_where('regencies', [
        'province_id' => $propinsi_id
    ]);

    if ($kabkota->result()) {
        return $kabkota->result();
    }

    return [];
}

/**
 * @param $kabupatekota_id
 * @return array
 */
function get_kecamatan($kabupatekota_id)
{
    $CI =& get_instance();
    $kecamatan = $CI->db->get_where('districts', [
        'regency_id' => $kabupatekota_id
    ]);

    if ($kecamatan->result()) {
        return $kecamatan->result();
    }

    return [];
}

/**
 * @param $kecamatan_id
 * @return array
 */
function get_kelurahan($kecamatan_id)
{
    $CI =& get_instance();
    $keluarahan = $CI->db->get_where('villages', [
        'district_id' => $kecamatan_id
    ]);

    if ($keluarahan->result()) {
        return $keluarahan->result();
    }

    return [];
}

/**
 * get location by id kelurahan
 * @param $id_kelurahan
 * @param string $name [propinsi, kabupatenkota, kecamatan]
 * @return object | False if not found $name
 */
function get_location_by_id_kel($id_kelurahan, $name = 'propinsi')
{
    $CI =& get_instance();

    switch ($name) {
        case 'propinsi' :
            $id_location = substr($id_kelurahan, 0, -8);
            return $this->general->get_propinsi($id_propinsi);
            break;
        case 'kabupatenkota':
            $id_location = substr($id_kelurahan, 0, -6);
            return $this->db->get_where('regencies', ['id' => $id_location]);
            break;
        case 'kecamatan' :
            $id_location = substr($id_kelurahan, 0, -3);
            return $this->db->get_where('districts', ['id' => $id_location]);
            break;
        case 'kelurahan' :
            return $this->db->get_where('villages', ['id' => $id_location]);
    }

    return false;
}

/**
 * @param $ustadz_id
 * @param string $term
 * @return string
 */
function get_ustadz_term($ustadz_id, $term = 'ustadz_name')
{
    $CI =& get_instance();
    $CI->db->select($term);
    $query = $CI->db->get_where('ustadz', [
        'ustadz_id' => $ustadz_id
    ]);

    $result = false;
    if ($query->num_rows() > 0) {
        $result = $query->result()[0]->$term;
    }

    $CI->db->reset_query();
    return $result;
}

/**
 * get meta user
 */
function get_user_by_id($user_id, $field = 'display_name')
{
    $CI =& get_instance();
    $user = $CI->general->get_user_by_ID($user_id, $field);
    return $user;
}

/**
 * @param $attachment
 * @return bool|string
 */
function get_thumbnail($attachment)
{
    if ($attachment) {
        $redirect_protocol = USE_SSL ? 'https' : NULL;
        return '<img src="' . site_url('media/attachment/' . $attachment, $redirect_protocol). '" alt="" />';
    }

    return false;
}

/**
 * @param $masjid_id
 * @return mixed
 */
function is_admin_masjid($masjid_id)
{
    $user_id = config_item('auth_user_id');
    $CI =& get_instance();
    return $CI->general->get_masjid_role($user_id, $masjid_id);
}
