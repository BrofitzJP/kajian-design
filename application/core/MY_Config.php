<?php
class MY_Config extends CI_Config
{
    public function __construct()
    {
        parent::__construct();
        $cacheMethod = null;
        if (extension_loaded('redis')) {
            try {
                $redis = new Redis();
                $redis->connect('127.0.0.1');
                $cacheMethod = 'redis';
                $redis->close();
                unset($redis);
            } catch (\Throwable $e) {

            }
        }
        if (!isset($cacheMethod) && extension_loaded('memcached')) {
            $cacheMethod = 'memcached';
        }

        $cacheMethod = $cacheMethod ?: 'file';
        $this->set_item('cache', array(
            'adapter' => $cacheMethod,
            'key_prefix' => 'ci_cache_'
        ));
    }
}