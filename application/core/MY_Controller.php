<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Community Auth - MY Controller
 *
 * Community Auth is an open source authentication application for CodeIgniter 3
 *
 * @package     Community Auth
 * @author      Robert B Gottier
 * @copyright   Copyright (c) 2011 - 2017, Robert B Gottier. (http://brianswebdesign.com/)
 * @license     BSD - http://www.opensource.org/licenses/BSD-3-Clause
 * @link        http://community-auth.com
 */

require_once APPPATH . 'third_party/community_auth/core/Auth_Controller.php';

/**
 * Class MY_Controller
 *
 * @property General $general
 * @property CI_Loader $load
 * @property CI_Cache $cache
 * @property CI_DB_query_builder $db
 * @property CI_Form_validation $form_validation
 */
class MY_Controller extends Auth_Controller
{
    protected $use_ssl;

    /**
     * Class constructor
     */
    public function __construct()
    {
        parent::__construct();
        // load cache
        $cacheConfig = config_item('cache');
        $cacheConfig =  !is_array($cacheConfig)
            ? array('adapter' => 'file', 'key_prefix' => 'ci_cache_')
            : $cacheConfig;
        $this->load->driver('cache', $cacheConfig);
        $this->use_ssl = USE_SSL ? 'https' : NULL;
    }
}

/* End of file MY_Controller.php */
/* Location: /community_auth/core/MY_Controller.php */