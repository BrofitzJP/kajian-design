<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Post_model extends MY_Model
{
    public function get_tos()
    {
        $tos_id = get_option('tos_page');
        $query = $this->db->get_where('posts', array(
            'ID' => $tos_id,
            'post_type' => 'page'
        ));

        $result = $query->result();

        if ( empty($result) ) {
            return false;
        }

        return $result;
    }
}