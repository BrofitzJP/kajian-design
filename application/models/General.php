<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Class General
 * @property CI_DB_query_builder $db
 * use $this->db->error() [array | object] to get error information
 */

class General extends MY_Model
{

    private $error;

    /**
     * --------------------------
     * User Handel
     * --------------------------
     */
    public function get_user_by_ID($user_id, $field = 'display_name')
    {
        $user = $this->db->get_where('users', [
            'user_id' => $user_id
        ]);

        if ($user->num_rows() > 0) {
            return $user->result()[0]->$field;
        }
        return false;
    }

    /**
     * @param array $args
     * @return mixed
     */
    public function get_users($args = [])
    {
        $default = [
            'not_in'  => false,
            'user_id' => false,
            'limit'   => 30,
            'page'    => 0,
            'where'   => []
        ];

        $args = array_merge($default, $args);

        $this->db->select('*');
        $this->db->from('users');

        if ($args['where'] && is_array($args['where'])) {
            foreach ($args['where'] as $where) {

                if (isset($where['field']) && $where['field'] &&
                    isset($where['value']) && $where['value']
                )  {
                    $this->db->where($where['field'], $where['value']);
                }
            }
        }
        // not in
        if ($args['not_in']) {
            $this->db->where_not_in('user_id', $args['not_in']);
        }

        $this->db->limit($args['limit'], $args['page']);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        }

        $this->error = $this->db->error();
        return false;
    }

    /**
     * get number of users
     * @return mixed
     */
    public function get_users_count()
    {
        $this->db->select('user_id');
        return $this->db->get('users')->num_rows();
    }

    /**
     * checking validation
     * @param $code
     * @return bool
     */
    public function _code_is_exist($code)
    {
        $this->db->select('user_id');
        $query = $this->db->get_where('users', array(
            'activation_code' => $code
        ));

        if ( empty( $query->result_array() ) ) {
            return false;
        }
        $rows = $query->result_array();
        $user_id = $rows[0]['user_id'];

        $user_data['banned'] = '0';
        $user_data['activation_code'] = '';
        $this->update_user($user_id, $user_data);

        return true;
    }

    /**
     * update user information
     * @param $user_id
     * @param array $user_data
     */
    private function update_user($user_id, $user_data = [])
    {
        $this->db->where('user_id', $user_id)
            ->update( $this->db_table('user_table'), $user_data );
    }

    /**
     * -----------------------
     * Location Handle
     * -----------------------
     */

    /**
     * get lokasi [provinces, regencies, district, villages]
     * @param array $args
     * @return bool|object|array
     *
     * $args = [
     *   'find' => [
     *     'field' => 'reg.name',
     *     'operator => 'like',
     *     'value => 'surakarta'
     *   ],
     *   'wheres' => [
     *       [
     *          'field' => 'pro.id',
     *          'value' => '35'
     *       ]
     *    ],
     *   'limit' => 30
     * ];
     *
     */
    public function get_lokasi($args = [])
    {
        $default = [
            'find' => [],
            'wheres' => [],
            'limit' => 20
        ];

        $args = array_merge($default, $args);

        $this->db->select('pro.id AS propinsi_id, 
                           pro.name AS propinsi, 
                           reg.id AS kabupatenkota_id,
                           reg.name AS kabupatenkota,
                           dis.id AS kecamatan_id,
                           dis.name AS kecamatan,
                           vil.id AS desa_id,
                           vil.name AS desa');
        $this->db->from('provinces pro');
        $this->db->join('regencies reg', 'pro.id = reg.province_id');
        $this->db->join('districts dis', 'reg.id = dis.regency_id');
        $this->db->join('villages vil', 'dis.id = vil.district_id');

        if (! empty($args['find']) && is_array($args['find'])) {
            $find = $args['find'];
            $operator = isset($find['operator']) ? $find['operator'] : '';
            switch ($operator) {
                case 'not_like':
                    $this->db->not_like($find['field'], $find['value']);
                    break;
                default:
                    $this->db->like($find['field'], $find['value']);
            }

        }

        if (! empty($args['wheres']) && is_array($args['wheres'])) {
            foreach ($args['wheres'] as $where) {
                $this->db->where($where['field'], $where['value']);
            }
        }

        if ($args['limit'] > 0) {
            $this->db->limit($args['limit']);
        }

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return false;
    }

    /**
     * get propinsi from table provinces
     * @param bool $propinsi_id
     * @return array
     */
    public function get_propinsi($propinsi_id = false)
    {
        if ($propinsi_id) {
            $get_propinsi = $this->db->get_where('provinces',[
                'id' => $propinsi_id
            ]);
        } else {
            $get_propinsi = $this->db->get('provinces');
        }
        if ($get_propinsi->result()) {
            $propinsi = $get_propinsi->result();
        } else {
            $propinsi = [];
        }

        return $propinsi;
    }

    /**
     * get kabupaten until propinsi
     * @param $kabupaten_id
     * @return bool|object|array
     */
    public function get_kabupatenkota_full($kabupaten_id)
    {
        $this->db->select('pro.id AS propinsi_id, pro.name AS propinsi, reg.id AS kabupatenkota_id, reg.name AS kabupatenkota');
        $this->db->from('regencies reg');
        $this->db->join('provinces pro', 'pro.id = reg.province');

        $this->db->where('reg.id', $kabupaten_id);

        $query = $this->db->get();
        if ($query->result()) {
            return $query->result();
        }

        return false;
    }

    /**
     * get Kecamatan until Propinsi
     * @param $kecamatan_id
     * @return bool|object|array
     */
    public function get_kecamatan_full($kecamatan_id)
    {
        $this->db->select('pro.id AS propinsi_id, pro.name AS propinsi, reg.id AS kabupatenkota_id, reg.name AS kabupatenkota,
        dis.id AS kecamatan_id, dis.name AS kecamatan');
        $this->db->from('districts dis');
        $this->db->join('regencies reg', 'reg.id = dis.regency_id');
        $this->db->join('provinces pro', 'pro.id = reg.province_id');

        $this->db->where('dis.id', $kecamatan_id);
        $kecamatan = $this->db->get();

        if ($kecamatan->result()) {
            return $kecamatan->result();
        }

        return false;
    }

    /**
     * get Kelurahan until Propinsi
     * @param $kelurahan_id
     * @return bool
     */
    public function get_kelurahan_full($kelurahan_id)
    {
        $kelurahan = $this->get_lokasi([
            'wheres' => [
                [
                    'field' => 'vil.id',
                    'value' => $kelurahan_id
                ],
            ],
            'limit' => -1
        ]);

        return $kelurahan;
    }

    /**
     * -----------------------
     * MOSQUE / Masjid
     * -----------------------
     * get Masjid
     */
    public function get_masjid($args = [])
    {

        $masjids = [];

        $default = [
            'select' => '*',
            'limit' => 30,
            'page' => 0,
            'find' => '',
            'where'=> [],
        ];

        $args = array_merge($default, $args);

        $this->db->select($args['select']);
        $this->db->from('mosque m');

        // where clause
        if (! empty($args['where']) && is_array($args['where'])) {
            foreach ($args['where'] as $row) {
                if (isset($row['field']) && $row['field'] && isset($row['value']) && $row['value']) {
                    $this->db->where($row['field'], $row['value']);
                }
            }
        }

        // find
        if ($args['find']) {
            $this->db->like('m.mosque_name', $args['find']);
        }

        $this->db->limit($args['limit'], $args['page']);
        $query = $this->db->get();

        if ($query->result()) {
            foreach ($query->result() as $res) {
                $location = [];
                switch (strlen($res->location)) {
                    case 2 :
                        $location = $this->get_propinsi($res->location);
                        break;
                    case 4 :
                        $location = $this->get_kabupatenkota_full($res->location);
                        break;
                    case 7 :
                        $location = $this->get_kecamatan_full($res->location);
                        break;
                    default:
                        $location = $this->get_kelurahan_full($res->location);
                }
                if (! empty($location)) {
                    $location = $location[0];
                }
                $masjids[] = (object) array_merge( (array) $res, ['location_full' => $location]);
            }
            return $masjids;
        }

        $this->error = $this->db->error();
        return false;
    }

    /**
     * get masjid Count
     * @return mixed
     */
    public function get_masjid_count()
    {
        $this->db->select('mosque_id');
        return $this->db->get('mosque')->num_rows();
    }

    /**
     * get spesific masjid with location of find by name
     * @param array $args['find' => '', 'where' => '']
     * @return bool
     */
    public function get_masjid_location($args = [])
    {
        return $this->get_masjid($args);
    }

    /**
     * getting full information of mosque
     * @param $masjid_id
     * @return mixed
     */
    public function get_masjid_data($masjid_id)
    {
        $masjid = [];
        $masjids = $this->get_masjid([
            'select' => '*',
            'where'  => [
                [
                    'field' => 'm.mosque_id',
                    'value' => $masjid_id
                ]
            ]
        ]);

        if (! $masjids) {
            return false;
        }

        $manager = [];
        foreach ($masjids as $row) {
            $manager['manager'] = $this->get_manager_by_ID($row->mosque_id);
            $masjid[] = (object) array_merge( (array) $row, (array) $manager);
        }

        return $masjid;
    }

    /**
     * get manager under mosque
     * @param $masjid_id
     * @return array
     */
    public function get_manager_by_ID($masjid_id)
    {
        $this->db->select('mm.manager_id, u.display_name, u.user_id, mm.role');
        $this->db->from('mosque_manager mm');
        $this->db->join('users u', 'u.user_id = mm.user_id');
        $this->db->where('mm.mosque_id', $masjid_id);

        $manager = $this->db->get();

        if ($manager->num_rows() > 0) {
            return $manager->result();
        }

        return [];
    }

    /**
     * check a user have been manager in current masjid,
     * untuk menghindari user yang sama memiliki 2 role yang berbeda
     * @param $masjid_id
     * @return bool
     */
    public function have_manager($masjid_id, $user_id)
    {
        $check = $this->db->get_where('mosque_manager', [
            'mosque_id' => $masjid_id,
            'user_id' => $user_id
        ]);

        if ($check->num_rows() > 0) {
            return true;
        }
        return false;
    }

    /**
     * getting role of mosque
     * @param $user_id
     * @param $masjid_id
     * @param bool $number_only
     * @return bool|string
     */
    public function get_masjid_role($user_id, $masjid_id, $number_only = true)
    {
        $user_role = $this->db->select('role')->get_where('mosque_manager', [
            'mosque_id' => $masjid_id,
            'user_id'   => $user_id
        ]);

        if ($user_role->num_rows() > 0) {
            $rows = $user_role->result()[0];
            $role = '';
            switch ($rows->role) {
                case '1' :
                    $role = ($number_only) ? '1' : 'Editor';
                    break;
                case '9' :
                    $role = ($number_only) ? '9' : 'Admin';
                    break;
            }

            return $role;
        }

        return false;
    }


    /**
     * ----------------------------
     * KAJIAN HANDLER
     * ----------------------------
     * get, update, delete
     */

    /**
     * Get All Kajian
     */
    public function get_kajian($args = [], $select = 'k.*, m.mosque_name, u.ustadz_name, us.display_name, m.location AS mosque_location')
    {
        $default_location = [
            'provinces' => false,
            'regencies' => false,
            'districts' => false,
            'villages'  => false,
            'mosque' => false,
        ];

        if (isset($args['location'])) {
            $args['location'] = array_merge($default_location, $args['location']);
        }

        $default_date = [
            'start' => null,
            'end'   => null
        ];

        if (isset($args['date'])) {
            $args['date'] = array_merge($default_date, $args['date']);
        }

        $default = [
            'posts_per_page' => 10,
            'page'   => 0,
            'location' => $default_location,
            'ustadz' => false,
            'days'   => false,
            'date' => $default_date,
            's' => null,
            'status' => '1'
        ];

        // merge default config
        $args = array_merge($default, $args);

        $this->db->select($select);
        $this->db->from('kajian k');
        $this->db->join('mosque m', 'm.mosque_id = k.location');
        $this->db->join('ustadz u', 'u.ustadz_id = k.ustadz_id');
        $this->db->join('users us', 'us.user_id = k.user_id');

        /**
         * Filter Location
         */
        if ($args['location']['provinces']) {
            $location = substr($args['location']['provinces'], 0, -6);

            if ($args['location']['regencies']) {
                $location = substr($args['location']['regencies'], 0, -6);

                if ($args['location']['districts']) {
                    $location = substr($args['location']['districts'], 0, -3);

                    if ($args['location']['villages']) {
                        $location = $args['location']['villages'];

                    }
                }
            }

            if ($args['location']['mosque']) {
                $this->db->where('k.location', $args['location']['mosque']);
            }

            // add to query
            $this->db->like('m.location', $location, 'after');
        }

        /**
         * Filter Status
         */
        if ($args['status']) {
            $this->db->where('k.status', $args['status']);
        }

        /**
         * Filter Search
         */
        if ($args['s']) {
            $this->db->like('k.tema', $args['s']);
        }
        /**
         * date filter
         */
        if ($args['date']['start']) {
            $this->db->where('k.date_time >=', $args['date']['start']);
        }

        if ($args['date']['end']) {
            $this->db->where('k.date_time <=', $args['date']['end']);
        }

        /**
         * Filter by Days
         */
        if ($args['days']) {
            $this->db->where('k.day', $args['days']);
        }

        /**
         * Filter by Ustadz
         */
        if ($args['ustadz']) {
            $this->db->where('k.ustadz_id', $args['ustadz']);
        }

        /**
         * LIMIT HANDLE
         */
        if ($args['posts_per_page'] >= 0) {
            $this->db->limit($args['posts_per_page'], $args['page']);
        }

        // query

        $query = $this->db->get();

        if ($query->result()) {
            return $query->result();
        } else {
            $this->error = $this->db->error();
        }

        return false;
    }

    /**
     * get row count of kajian
     * @return mixed
     */
    public function get_count_kajian()
    {
        return $this->db->get_where('kajian', [
            'status' => '1'
        ])->num_rows();
    }

    /**
     * get spesifik kajian
     * @param $kajian_id
     * @return bool|array
     */
    public function get_kajian_by_id($kajian_id)
    {
        $select = 'k.*, m.mosque_name, u.ustadz_name, us.display_name, m.location AS mosque_location';

        $this->db->select($select);
        $this->db->from('kajian k');
        $this->db->join('mosque m', 'm.mosque_id = k.location');
        $this->db->join('ustadz u', 'u.ustadz_id = k.ustadz_id');
        $this->db->join('users us', 'us.user_id = k.user_id');

        $this->db->where('k.kajian_id', $kajian_id);

        $query = $this->db->get();

        if ($query) {
            return $query->result();
        }

        return false;
    }

    /**
     * get filter data like propinsi, kabupatenkota, kecamatan, kelurahan
     * ustadz, masjid
     * @param array $args
     * @return array
     */
    public function get_filter_kajian($args = [])
    {
        $default_location = [
            'provinces' => false,
            'regencies' => false,
            'districts' => false,
            'villages'  => false,
            'mosque' => false,
        ];

        $location = $default_location;
        if (isset($args['location'])) {
            $args['location'] = array_merge($default_location, $args['location']);
        }

        $default = [
            'posts_per_page' => -1,
            'paged'   => 0,
            'location' => $location,
            'ustadz' => false,
            'days'   => false,
            'date' => [
                'start' => null,
                'end'   => null
            ],
            's' => null,
            'status' => '1'
        ];

        $args = array_merge($default, $args);
        $result = $this->get_kajian($args, 'k.day, u.ustadz_id, m.mosque_id, m.location AS m_location');

        $propinsi = $kabkota = $kec = $kel = $days = $mosques = $ustadz  = [];

        if ($result) {
            foreach ($result as $row) {
                // check filter
                $propinsi[] = substr($row->m_location, 0, -8);

                if ($args['location']['provinces']) {
                    $kabkota[] = substr($row->m_location, 0, -6);

                    if ($args['location']['regencies']) {
                        $kec[] = substr($row->m_location, 0, -3);

                        if ($args['location']['districts']) {
                            $kel[] = $row->m_location;
                            $mosques[] = $row->mosque_id;
//                            if ($args['location']['villages']) {
//
//                            }
                        }
                    }
                }


                if ( isset($args['days']) ) {
                    $days[] = $row->day;
                }

                if (isset($args['ustadz'])) {
                    $ustadz[]  = $row->ustadz_id;
                }
            }
        }


        $meta_data = [
            'location' => [
                'pro' => array_count_values($propinsi),
                'kabkota' => array_count_values($kabkota),
                'kec' => array_count_values($kec),
                'kel' => array_count_values($kel),
            ],
            'days' => array_count_values($days),
            'mosques' => array_count_values($mosques),
            'ustadz'  => array_count_values($ustadz)
        ];

        return $meta_data;
    }

    /**
     * @param $id_kajian
     * @param $args
     * @return bool
     */
    public function update_kajian($id_kajian, $args)
    {
        $this->db->where('kajian_id', $id_kajian);
        $update = $this->db->update('kajian', $args);
        if ($update) {
            return true;
        }

        show_error($this->db->error(), 200, 'Error Update');
        return false;
    }

    /**
     * ---------------------------------
     * Ustadz Handler
     * ---------------------------------
     */

    /**
     * get ustadz detail
     * @param array $args
     * @return bool|array
     */
    public function get_ustadz($args = [])
    {
        $default = [
            'select' => '*',
            'ustadz_id' => '',
            'name'  => '',
            'limit' => 30,
            'page' => 0,
            'status' => '1'
        ];

        $args = array_merge($default, $args);

        $this->db->select($args['select']);
        $this->db->from('ustadz');

        if ( $args['name'] ) {
            $this->db->like('ustadz_name', $args['name']);
        }

        if ( $args['ustadz_id'] ) {
            $this->db->where('ustadz_id', $args['ustadz_id']);
        }

        if ( $args['status'] ) {
            $this->db->where('status', $args['status']);
        }

        $this->db->limit($args['limit'], $args['paged']);

        $query = $this->db->get();

        if ($query->result()) {
            return $query->result();
        }


        return false;
    }

    /**
     * get ustadz count
     * @return integer
     */
    public function get_ustadz_count()
    {
        $this->db->select('ustadz_id');
        $query = $this->db->get('ustadz');
        return $query->num_rows();
    }

    /**
     * ---------------------------------
     * Utility
     * ---------------------------------
     */
    /**
     * Display Error
     */
    public function get_error()
    {
        print_r($this->error);
    }

}