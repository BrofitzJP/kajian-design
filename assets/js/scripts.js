function send_ajax(url, value, target) {
        var list = '';
        $.ajax({
            url: url,
            data: value,
            dataType: 'json',
            success: function (respond) {
                target.html('');
                if (typeof respond.error === 'undefined') {
                    $.each(respond, function (index, val) {
                        list += '<option value="'+ val.id +'">' + val.name + '</option>';
                    });
                } else {
                    list = '<option value="">Not Found<option>';
                }

                target.html(list);
            }
        });
    }
